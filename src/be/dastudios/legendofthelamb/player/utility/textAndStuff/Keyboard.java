package be.dastudios.legendofthelamb.player.utility.textAndStuff;

import be.dastudios.legendofthelamb.Game;

import java.io.Serializable;
import java.util.InputMismatchException;
import java.util.Scanner;
/**
 * This is a utility class that makes a keyboard for our users to use as input.
 * it also checks for exceptions so users can't enter wrong characters.
 */

public class Keyboard implements Serializable {
    static Scanner keyboard = new Scanner(System.in);
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static int readInt(String text) {
        System.out.println(text);
        while (true) {
            try {
                int result = Integer.parseInt(keyboard.nextLine());
                return result;
            } catch (NumberFormatException nfe) {
                System.out.println("Please enter a number.");

            }
        }
    }

    public static String readLine(String text) {
        System.out.println(text);
        while (true) {
            try {
                String line = keyboard.nextLine();
                line.toLowerCase();
                return line;
            } catch (InputMismatchException ime) {
                System.out.println("Please type in words.");

            }
        }
    }


    public static void closeKeyboard() {
        keyboard.close();
    }

}
