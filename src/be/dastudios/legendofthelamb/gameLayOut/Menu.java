package be.dastudios.legendofthelamb.gameLayOut;

import be.dastudios.legendofthelamb.Game;
import be.dastudios.legendofthelamb.itemEncounter.FindItem;
import be.dastudios.legendofthelamb.map.ForestOfStreams;
import be.dastudios.legendofthelamb.player.Player;
import be.dastudios.legendofthelamb.combatSystem.Combat;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.AskInput;

import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Random;
import java.util.Scanner;

public class Menu implements Serializable {
 private static Random random = new Random();
 private static AskInput input = new AskInput();
 private static Scanner keyboard= new Scanner (System.in);
 private static String choice;
 private static Player player;
 private static ForestOfStreams map;
 private static Game game;


    public static void startMenu() {
        System.out.println("++++++++++++++++++++++++++++++\n" +
                "++ Legend of the Lamb - DA  ++\n" +
                "++++++++++++++++++++++++++++++\n" +
                "---   DA Studios Belgium   ---\n" +
                "------------------------------\n" +
                "Loading Maps and Savegames\n" +
                "++++++++++\n" +
                "++ DONE ++\n" +
                "++++++++++\n");

        choice = input.getChoiceMainMenu();
        while (!(choice.equals("quit"))) {
            if (choice.equals("new")) {
                String name = input.askName();
                Player player = new Player(name);
                setPlayer(player);
                ForestOfStreams map = new ForestOfStreams();
                setMap(map);
                setThisGame(new Game(player, map));
                System.out.println("Player has been created.\n");
                System.out.println(player);
                input.continueInGame();
                System.out.println("Welcome in Letla, a world full of magical items and beings.\n" +
                        "You are about to embark on a life-changing adventure.\n" +
                        "You might be lucky and find treasures on your path.\n" +
                        "But beware, for you might encounter creatures like the Bugbear or the \n" +
                        "Hobgoblin who will not hesitate to tear you apart!\n" +
                        "It's dangerous to go alone, take this:\n");
                player.getBackpack().printBackPack();
                System.out.println("Good luck, brave traveler and be safe!\n");
                input.continueInGame();
//                TODO starterszone hier met keuze naar ForestofStreams of CaveOfThreads (cave nog uit te werken)
//                enterWorld(player);
                enterForestOfStreams(player, map);

                choice = input.getChoiceMainMenu();

            }
            else if (choice.equals("load")) {
                try {
                    if(Game.printSavegames()) {
                        setThisGame(Game.loadGame());
                        Menu.getMap().walk();
                        startWalking(Menu.getPlayer(), Menu.getMap(), new Combat());
                        choice = input.getChoiceMainMenu();
                    } else {
                        choice = input.getChoiceMainMenu();
                    }
                } catch (Exception e){
                    System.out.println("There is no saved game with that name.");
                    choice = input.getChoiceMainMenu();
                }
            }
            else if (choice.equals("delete")){
                deleteGame();
                choice = input.getChoiceMainMenu();
            }
            else if (choice.equals("settings")) {
                System.out.println("What do you mean settings? You need help with reading?");
                choice = input.getChoiceMainMenu();
            }

            else if (choice.equals("controls")) {
                System.out.println("Just follow the directions printed out during the game. It's not hard.");
                choice = input.getChoiceMainMenu();
            }
            else {
                System.out.println("Please type in 'new', 'load', 'delete', 'controls', 'settings' or 'quit'");
                choice = input.getChoiceMainMenu();
            }
        }
        System.out.println("Quitting the game...");
        keyboard.close();
    }

    private static ForestOfStreams getMap() {
        return map;
    }

    public static void setMap(ForestOfStreams map) {
        Menu.map = map;
    }

    private static void enterForestOfStreams(Player player, ForestOfStreams map) {
        Combat combat = new Combat();
        map.startTile();
        startWalking(player, map, combat);
    }

    private static void startWalking(Player player, ForestOfStreams map, Combat combat) {
        System.out.println(map.getTile());
        int direction = map.getWalkChoice();
        while (player.getHp() > 0 && (map.getTile() != map.endTile()) && direction != 0){
            map.walk();
            direction = map.getWalkChoice();
            if (direction != 0) {
                if (map.isGoblinMinionTile()) {
                    combat.generateGoblinMinion();
                    combat.enterCombat(player);
                }
                if (map.isGoblinFighterTile()) {
                    combat.generateGoblinFighter();
                    combat.enterCombat(player);
                }
                if (map.isItemTile()){
                    FindItem.seeItem(player);

                }
            }
        }
    }

    public static Player getPlayer() {
        return player;
    }

    public static void setPlayer(Player player) {
        Menu.player = player;
    }

    public static Game getGame() {
        return game;
    }

    public static void deleteGame() {
        if (Game.printSavegames()) {
            String name = input.chooseSavegameToDelete();
            try {
                Files.delete(Path.of(name + ".sav"));
                System.out.println(name + " has been deleted.");
            } catch (Exception e) {
                System.out.println("No saved games have been found.");
            }
        }
    }

    public static void setThisGame(Game game) {
        Menu.game = game;
        Menu.player = game.getPlayer();
        Menu.map = game.getMap();
    }

    //    private static void enterWorld(Player player) {
//        Map map = new Map();
//        Combat combat = new Combat();
//        int direction = map.getChoice();
//        while (player.getHp() > 0 && direction != 0) {
//            int steps = random.nextInt(6);
//                for (int i = 0; i < steps; i++) {
//                    if (direction != 0) {
//                        map.walk();
//                        direction = map.getChoice();
//                    }
//                }
//            if(direction != 0) {
//                combat.enterCombat(player);
//            }
//        }
//    }
}


