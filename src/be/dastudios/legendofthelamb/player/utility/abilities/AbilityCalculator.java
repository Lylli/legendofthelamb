package be.dastudios.legendofthelamb.player.utility.abilities;

import be.dastudios.legendofthelamb.Game;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;


/**
 * This is a class to calculate the variable array during character creation if the player doesn't want
 * to use the standard abilityvariables.
 */
public class AbilityCalculator implements Serializable {
    private static ArrayList<Integer> values = new ArrayList<>();
    private static ArrayList<Integer> finalValues = new ArrayList<>();


    public ArrayList<Integer> finalVariableValues(){
        for (int j = 0; j<8; j++){
            int a = createAndAddValues();
            finalValues.add(a);
            values.clear();
        }
        finalValues.sort(Comparator.reverseOrder());
        finalValues.remove(0);
        finalValues.remove(6);
        return finalValues;
    }

    private int createAndAddValues() {
        createValues();
       return addedValues();
    }


    public ArrayList<Integer> createValues() {
        for (int i = 0; i < 4; i++) {
            Dice dice = new Dice();
            values.add(dice.rollD6());
        }
        values.sort(Comparator.reverseOrder());
        return values;
    }

    public int addedValues(){
        int finalValue = values.get(0) + values.get(1) + values.get(2);
        return finalValue;
    }
}
