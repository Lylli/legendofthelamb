package be.dastudios.legendofthelamb.player.gameclass;

import be.dastudios.legendofthelamb.monsters.Monster;
import be.dastudios.legendofthelamb.player.Player;
import be.dastudios.legendofthelamb.player.utility.abilities.AbilityModifier;
import be.dastudios.legendofthelamb.player.utility.abilities.Dice;
import be.dastudios.legendofthelamb.player.utility.abilities.AbilityCalculator;
import be.dastudios.legendofthelamb.player.backpack.RegularBackPack;
import be.dastudios.legendofthelamb.player.backpack.items.armor.ChainMail;
import be.dastudios.legendofthelamb.player.backpack.items.weapons.Sword;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.Keyboard;

import java.io.Serializable;
import java.util.ArrayList;

public class Fighter extends GameClass {
    private final String nameClass = "Fighter";
    private int lifeDiceOutcome;
    private int armourBase = 8;
    private int choiceAttackLevel5;
    private int level = 1;

    private int strength;
    private int constitution;
    private int wisdom;
    private int charisma;
    private int dexterity;
    private int intelligence;

    private RegularBackPack backpack = new RegularBackPack();
    private int wallet;

    public Fighter (){
        this.lifeDiceOutcome = getHP();
        this.backpack = startersBackpack();

    }

    public RegularBackPack startersBackpack(){
        Sword sword = new Sword();
        ChainMail chainMail = new ChainMail();
        this.armourBase += chainMail.getArmourBaseChainMail();
        this.backpack.addItemInBackPack(sword);
        this.backpack.addItemInBackPack(chainMail);
        this.wallet = 15;
        this.backpack.addGpInBackPack(wallet);
        return backpack;
    }


    public RegularBackPack getBackpack() {
        return backpack;
    }

    public String getNameClass() {
        return nameClass;
    }

    public int getHP(){
        int total = 0;
        for (int k = 0; k<3; k++){
            Dice lifeDice = new Dice();
            total += lifeDice.rollD12();
        }
        return total +10;
    }
    public int getArmourBase(){
        return armourBase;
    }

    public int getLifeDiceOutcome() {
        return lifeDiceOutcome;
    }

    public void setBackpack(RegularBackPack backpack) {
        this.backpack = backpack;
    }


    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public void setStrength(int strength) {
        this.strength = strength;
    }

    @Override
    public int getConstitution() {
        return constitution;
    }

    @Override
    public void setConstitution(int constitution) {
        this.constitution = constitution;
    }

    @Override
    public int getWisdom() {
        return wisdom;
    }

    @Override
    public void setWisdom(int wisdom) {
        this.wisdom = wisdom;
    }

    @Override
    public int getCharisma() {
        return charisma;
    }

    @Override
    public void setCharisma(int charisma) {
        this.charisma = charisma;
    }

    @Override
    public int getDexterity() {
        return dexterity;
    }

    @Override
    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    @Override
    public int getIntelligence() {
        return intelligence;
    }

    @Override
    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public void setAbilityValuesStandard(){
        this.strength = 16 + 2;
        this.constitution = 14 + 1;
        this.wisdom = 14;
        this.charisma = 13;
        this.dexterity = 11;
        this.intelligence = 8;
    }

    public void setAbilityValuesVariable() {
        AbilityCalculator ac = new AbilityCalculator();
        ArrayList<Integer> abilityValues = ac.finalVariableValues();
        strength = abilityValues.get(0) + 2;
        constitution = abilityValues.get(1) + 1;
        wisdom = abilityValues.get(2);
        charisma = abilityValues.get(3);
        dexterity = abilityValues.get(4);
        intelligence = abilityValues.get(5);
    }
//TODO voor elke klasse kunnen we de aanvallen en damage apart doen => meer klassen aanmaken bv klasse fighterTriesToAttack => attack() en swordAttack daarin
//    en hierbinnen aanroepen en dan klasse fighterDamage met de levels en diens aanvallen. En dit dan voor elke klasse
    public int attack(){
        Keyboard keyboard = new Keyboard();
        int totalAttack = 0;
        switch (getLevel()){
            case 1:
            case 2:
            case 3:
            case 4:
                totalAttack = swordAttack();
                break;
            case 5:
                this.choiceAttackLevel5 = keyboard.readInt("Please choose your attack: \n" +
                        "1. Sword attack\n" +
                        "2. Sword Dance\n" +
                        "3. Sword Dance Enhanced\n" +
                        "4. Mighty Zornhau\n");
                while (choiceAttackLevel5 < 1 || choiceAttackLevel5 > 4){
                    System.out.println("Please choose a valid number.\n");
                    this.choiceAttackLevel5 = keyboard.readInt("Please choose your attack: \n" +
                            "1. Sword attack\n" +
                            "2. Sword Dance\n" +
                            "3. Sword Dance Enhanced\n" +
                            "4. Mighty Zornhau\n");
                }
                if (choiceAttackLevel5 != 4){
                    totalAttack = swordAttack();
                } else {
                    Dice dice = new Dice();
                    Dice dice2 = new Dice();
                    AbilityModifier abilityModifier = new AbilityModifier(getStrength());
                    int levelAttack = Math.min(dice.rollD20(), dice2.rollD20()) + getProficiency() + abilityModifier.useModifier();
                    int sword = dice.rollD20() + getProficiency() + getStrength();
                    totalAttack = (sword + levelAttack) / 2;
                }
                break;
        } return totalAttack;
    }

    private int swordAttack() {
        int totalAttack;
        Dice dice = new Dice();
        AbilityModifier abilityModifier = new AbilityModifier(getStrength());
        int levelAttack = dice.rollD20() + getProficiency() + abilityModifier.useModifier();
        int sword = dice.rollD20() + getProficiency() + getStrength();
        totalAttack = (sword + levelAttack)/2;
        return totalAttack;
    }

    private int swordDamage(){
        int totalDamage;
        Dice dice = new Dice();
        AbilityModifier abilityModifier = new AbilityModifier(getStrength());
        int levelDamage = 0;
        do {
            levelDamage = dice.rollD10() + abilityModifier.useModifier();
        } while (levelDamage == 0);
        int sword = dice.rollD10() + getStrength();
        totalDamage = sword / levelDamage;
        System.out.println("You hit with the sword! Tjakaa!!");
        return totalDamage;
    }

    public int damage(Monster monster){
        Keyboard keyboard = new Keyboard();
        int totalDamage = 0;
        switch (getLevel()){
            case 1:
            case 2:
                totalDamage = swordDamage();
                break;
            case 3:
                int attackChoice = keyboard.readInt("Please choose your attack: \n" +
                        "1. Sword attack\n" +
                        "2. Sword dance\n");
                while (attackChoice < 1 || attackChoice > 2){
                    System.out.println("Please choose a valid number.\n");
                    attackChoice = keyboard.readInt("Please choose your attack: \n" +
                            "1. Sword attack\n" +
                            "2. Sword dance\n");
                }
                if (attackChoice == 1){
                    totalDamage = swordDamage();
                } else {
                    totalDamage = swordDamage() + swordDamage();
                }
                break;
            case 4:
                attackChoice = keyboard.readInt("Please choose your attack: \n" +
                        "1. Sword attack\n" +
                        "2. Sword Dance\n" +
                        "3. Sword Dance Enhanced\n");
                while (attackChoice < 1 || attackChoice > 3){
                    System.out.println("Please choose a valid number.\n");
                    attackChoice = keyboard.readInt("Please choose your attack: \n" +
                            "1. Sword attack\n" +
                            "2. Sword Dance\n" +
                            "3. Sword Dance Enhanced\n");
                }
                if (attackChoice == 1){
                    totalDamage = swordDamage();
                } else if(attackChoice == 2){
                    totalDamage = swordDamage() + swordDamage();
                } else {
                    totalDamage = swordDamage() + swordDamage() +swordDamage();
                }
                break;
            case 5:
                if(choiceAttackLevel5 == 4) {
                    totalDamage = 1000000000;
                    System.out.println("Monster has been one-shotted, nice GG OP");
                } else if (choiceAttackLevel5 == 1){
                    totalDamage = swordDamage();
                } else if(choiceAttackLevel5 == 2){
                    totalDamage = swordDamage() + swordDamage();
                } else {
                    totalDamage = swordDamage() + swordDamage() +swordDamage();
                }
                break;


        } return totalDamage;
    }

    @Override
    public int getLevel() {
        return level;
    }

    @Override
    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public int getWeaponChoice() {
        return 0;
    }

    @Override
    public void setWeaponChoice() {

    }

    @Override
    public int heal() {
        return 0;
    }

    @Override
    public String toString() {
        return "Fighter{" +
                "nameClass='" + nameClass + '\'' +
                ", armourbase = " + armourBase +
                ", lifeDiceOutcome=" + lifeDiceOutcome +
                ", strength=" + strength +
                ", constitution=" + constitution +
                ", wisdom=" + wisdom +
                ", charisma=" + charisma +
                ", dexterity=" + dexterity +
                ", intelligence=" + intelligence +
                '}';
    }
}
