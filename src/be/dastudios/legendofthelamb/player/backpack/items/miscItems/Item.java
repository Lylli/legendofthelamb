package be.dastudios.legendofthelamb.player.backpack.items.miscItems;

import be.dastudios.legendofthelamb.Game;

import java.io.Serializable;

/**
 * This is a superclass for the items in the game, so we can put these different items in backpacks, give them to monsters to be available as loot,
 * have them as a reward for quests (not yet available) and so on
 * Items are armor, weapons and potions (not yet available)
 */
public class Item implements Serializable {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Item{" +
                "name='" + name + '\'' +
                '}';
    }
}
