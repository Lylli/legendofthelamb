package be.dastudios.legendofthelamb.player.utility.textAndStuff;

import be.dastudios.legendofthelamb.Game;

import java.io.Serializable;

/**
 * This is a class where the user gives input that might be asked in the game to set variables.
 * Remark: not everything is listed here yet.
 * @see Keyboard
 */

public class AskInput implements Serializable {
   Keyboard keyboard = new Keyboard();


    public int askDuringCombatChoice() {
        int choice = keyboard.readInt("What do you wanna do?" + "\n" +
                "1. Attack" + "\n" +
                "2. Action" + "\n" +
                "3. Run away like the coward you are...");

        while (choice < 1 || choice > 3){
            System.out.println("Please choose a valid number.");
            choice = keyboard.readInt("What do you wanna do?" + "\n" +
                    "1. Attack" + "\n" +
                    "2. Action" + "\n" +
                    "3. Run away like the coward you are...");
        }
        return choice;
    }

    public int askHealOrAttack(){
        int choice = keyboard.readInt("What do you wanna do?" + "\n" +
                "1. Attack" + "\n" +
                "2. Heal" + "\n" +
                "3. Action" + "\n" +
                "4. Run away like the coward you are...");

        while (choice < 1 || choice > 4){
            System.out.println("Please choose a valid number.");
            choice = keyboard.readInt("What do you wanna do?" + "\n" +
                    "1. Attack" + "\n" +
                    "2. Heal" + "\n" +
                    "3. Action" + "\n" +
                    "4. Run away like the coward you are...");
        }
        return choice;
    }

    public int askItemPickUp(){
        int choice = keyboard.readInt("Would you like to examine it?" + "\n" +
                "1. Yes, please!" + "\n" +
                "2. No, it looks filthy!");
        while (choice < 1 || choice > 2){
            System.out.println("Please choose a valid number.");
            choice = keyboard.readInt("Would you like to examine it?" + "\n" +
                    "1. Yes, please!" + "\n" +
                    "2. No, it looks filthy!");
        }
        return choice;
    }

    public int askItemKeep(){
        int choice = keyboard.readInt("Would you like to keep it?" + "\n" +
                "1. Yes! More possessions!!" + "\n" +
                "2. No, somebody obviously lost this and I ain't no thief!");
        while (choice < 1 || choice > 2){
            System.out.println("Please choose a valid number.");
            choice = keyboard.readInt("Would you like to keep it?" + "\n" +
                    "1. Yes! More possessions!!" + "\n" +
                    "2. No, somebody obviously lost this and I ain't no thief!");
        }
        return choice;
    }



    public int askWhereToWalk(){
        int choice= keyboard.readInt("Which direction would you like to walk?\n" +
                "1. North\n" +
                "2. East\n" +
                "3. South\n" +
                "4. West\n" +
                "5. Show map\n" +
                "0. Save and exit\n");
        return choice;
    }

    public void continueInGame() {
        String next = keyboard.readLine(LayOutText.pressAnyKeyToContinue());
    }

    public String askName(){
        String name = keyboard.readLine("Enter your name:");
        return name;
    }

    public String getChoiceMainMenu(){
        String choiceMainMenu = keyboard.readLine(LayOutText.mainMenu());
        choiceMainMenu= choiceMainMenu.toLowerCase();
        return choiceMainMenu;
    }

    public String getNameSavegame(){
        String name = keyboard.readLine("Enter the name of your savegame");
        return name;
    }

    public String chooseSavegame(){
        String name = keyboard.readLine("Choose the savegame you wish to load");
        return name;
    }

    public String chooseSavegameToDelete(){
        String name = keyboard.readLine("Choose the savegame you wish to delete");
        return name;
    }

}
