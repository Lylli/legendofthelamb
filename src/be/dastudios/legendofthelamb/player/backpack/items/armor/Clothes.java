package be.dastudios.legendofthelamb.player.backpack.items.armor;

import be.dastudios.legendofthelamb.player.backpack.items.miscItems.Item;

public class Clothes extends Item {
    private String name = "clothes";
    private String description = "A linen or woollen tunic that will rip with the slightest touch.";
    private int armourBaseClothes = 4;

    public int getArmourBaseClothes() {
        return armourBaseClothes;
    }

    @Override
    public String toString() {
        return " " + name + " - description: " + description;
    }
}
