package be.dastudios.legendofthelamb.gameLayOut;

import be.dastudios.legendofthelamb.Game;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.AskInput;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.LayOutText;

import java.io.Serializable;
import java.util.Random;
import java.util.Scanner;

public class Map implements Serializable {
    private int choice = 1;

    public int getChoice() {
        return choice;
    }

    public void walk() {

        AskInput input = new AskInput();
        choice = input.askWhereToWalk();
        while (choice < 0 || choice > 4){
            System.out.println("PLease enter a correct number.");
            choice = input.askWhereToWalk();
        }

        switch (choice) {
                case 1:
                    System.out.println("You took a step towards the icy winds of the North.");
                    break;
                case 2:
                    System.out.println("You took a step towards the sapphire blue ocean of the East.");
                    break;
                case 3:
                    System.out.println("You took a step towards the blistering desert of the South.");
                    break;
                case 4:
                    System.out.println("You took a step towards the misty mountains of the West.");
                    break;
                case 0:
                    System.out.println("Thank you for visiting Letla, please come again!");
                    LayOutText.prettyLines();
                    break;
            }
        }
    }
