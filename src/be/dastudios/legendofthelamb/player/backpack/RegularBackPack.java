package be.dastudios.legendofthelamb.player.backpack;


import be.dastudios.legendofthelamb.player.backpack.items.miscItems.Item;
import be.dastudios.legendofthelamb.player.backpack.items.miscItems.Spellbook;

import java.util.ArrayList;
import java.util.List;
/**
 * This is a regular backpack for non-spellcasting classes.
 * You can hold regular items in it and gold pieces.
 */
public class RegularBackPack extends BackPack {
    private final int MAX_ITEMS = 20;
    private final int MAX_GP = 500;
    private String name = "Regular Backpack";
    private String description = "'Sure, I'll carry EvErYtHiNg'";
    List<Item> backpack = new ArrayList<>();
    int wallet;
    Item item;
    int gp;

    public void addItemInBackPack(Item item){
        if (backpack.size() < MAX_ITEMS){
            backpack.add(item);
        }
        else {
            System.out.println("I am sworn to carry your burdens. However I can't carry anymore.");
        }
    }
    @Override
    public void addGpInBackPack(int gp){
        if (wallet < MAX_GP) {
            if (gp > MAX_GP) {
                System.out.println("The reward is too big. LOL!"+"\n"+
                        "Retracting till total is valid.");
                this.wallet = MAX_GP;
            } else {
                this.wallet += gp;
            }
        }
        else {
            System.out.println("You are too rich, go spend some gold pieces.");
        }
    }

    @Override
    public void printWallet() {
        System.out.println("You have now " + this.wallet + " gold coins!");
    }

    @Override
    public void addSpellbookInPackOfKnowledge(Spellbook sb) {

    }

    @Override
    public void seePackOfKnowledge() {

    }

    public void printBackPack(){
        System.out.println(name + "\n" +
                "(Oh dear, it talks):" + "\n" +
                description + "\n" +
                "Contents: ");
        backpack.forEach(System.out::println);
        System.out.println("Gold: " + wallet + " gp");
    }

    public List<Item> seeInventory(){
        return backpack;
    }

    public int seeTotalCoins(){
        return wallet;
    }

    @Override
    public String toString() {
        return name + "\n" ;
    }
}
