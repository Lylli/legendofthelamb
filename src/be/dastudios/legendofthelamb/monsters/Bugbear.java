package be.dastudios.legendofthelamb.monsters;

import be.dastudios.legendofthelamb.player.utility.abilities.Dice;
import be.dastudios.legendofthelamb.player.backpack.items.armor.LeatherArmour;
import be.dastudios.legendofthelamb.player.backpack.items.miscItems.Item;
import be.dastudios.legendofthelamb.player.backpack.items.weapons.Sword;

import java.util.ArrayList;
import java.util.List;

public class Bugbear extends Monster {
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RESET = "\u001B[0m";
    private Dice dice;
    private int attackChance;
    private int damage;
    private List<Item> items = new ArrayList<>();
//    private int wallet;

    public Bugbear(){
        setName("Bugbear");
        setArmourClass(18);
        setHp(34);
        setCr(3);
        Sword sword = new Sword();
        LeatherArmour leatherArmour = new LeatherArmour();
        items.add(sword);
        items.add(leatherArmour);
        setWallet(5);
    }

    public void seeInventory(){
        items.forEach(System.out::println);
    }

    public int attack() {
        dice = new Dice();
        int totalAttack = dice.rollD20() + 5;
        System.out.println(ANSI_GREEN + " Bugbear " + ANSI_RESET + " is trying to attack!");
        return totalAttack;
    }

    public int damage(){
        dice = new Dice();
        int totalDamage = dice.rollD10() + 4;
        System.out.println("The " + ANSI_GREEN + " bugbear " + ANSI_RESET + " makes a pirouette and slashes twice!");
        return totalDamage * 2;
    }


    @Override
    public String toString() {
        return "You see a tall humanoid in front of you. Coarse hair covers most of its body. " +"\n"+
                "Its mouth is full of long, sharp fangs, and its nose is much like that of a bear." + "\n"+
                "------------------------------------" + "\n"+
                "You are encountering a bugbear! Stats: " + "\n" +
                "ArmourClass: " + getArmourClass() +", HP: " + getHP() +
                ", CR: " + getCr();
    }
}


