package be.dastudios.legendofthelamb.monsters;

import be.dastudios.legendofthelamb.player.utility.abilities.Dice;
import be.dastudios.legendofthelamb.player.backpack.items.armor.LeatherArmour;
import be.dastudios.legendofthelamb.player.backpack.items.miscItems.Item;
import be.dastudios.legendofthelamb.player.backpack.items.weapons.Sword;

import java.util.ArrayList;
import java.util.List;

public class GoblinFighter extends Monster {
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RESET = "\u001B[0m";
    private Dice dice;
    private int attackChance;
    private int damage;
    private List<Item> items = new ArrayList<>();
//    private int wallet;

    public GoblinFighter(){
        setName("Goblin Fighter");
        setArmourClass(17);
        setHp(15);
        setCr(1);
        Sword sword = new Sword();
        LeatherArmour leatherArmour = new LeatherArmour();
        items.add(sword);
        items.add(leatherArmour);
        setWallet(5);
    }

    public void seeInventory(){
        items.forEach(System.out::println);
    }

    public int attack() {
        dice = new Dice();
        int totalAttack = dice.rollD20() + 4;
        System.out.println(ANSI_GREEN + " Goblin Fighter " + ANSI_RESET + "  is trying to attack!");
        return totalAttack;
    }

    public int damage(){
        dice = new Dice();
        int totalDamage = dice.rollD6() + 3;
        System.out.println(ANSI_GREEN + " Goblin Fighter " + ANSI_RESET + " sure knows how to use that sword!");
        return totalDamage;
    }

    @Override
    public String toString() {
        return "That goblin is looking mean... Wait, is that a sword?!" + "\n"+
                "------------------------------------" + "\n"+
                "You are encountering a goblin fighter! Stats: " + "\n" +
                "ArmourClass: " + getArmourClass() +", HP: " + getHP() +
                ", CR: " + getCr();
    }
}
