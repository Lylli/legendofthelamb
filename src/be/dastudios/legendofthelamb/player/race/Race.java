package be.dastudios.legendofthelamb.player.race;

import be.dastudios.legendofthelamb.Game;

import java.io.Serializable;

/**
 * This is a superclass for the different kind of races in the game:
 * at the moment there are 3 different races: Human, Dwarf and Elf.
 * There is the option to add more races in a later stadium of the gamedevelopement.
 * Each race has extra bonusses on their abilities.
 */
public class Race implements Serializable {
   private String name;
   private int speed;
   private int strength;
   private int constitution;
   private int wisdom;
   private int charisma;
   private int dexterity;
   private int intelligence;


   public void setSpeed (int speed){
       this.speed = speed;
   }

    public int getSpeed() {
        return speed;
    }

    public String getName(){
        return name;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getConstitution() {
        return constitution;
    }

    public void setConstitution(int constitution) {
        this.constitution = constitution;
    }

    public int getWisdom() {
        return wisdom;
    }

    public void setWisdom(int wisdom) {
        this.wisdom = wisdom;
    }

    public int getCharisma() {
        return charisma;
    }

    public void setCharisma(int charisma) {
        this.charisma = charisma;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }
}
