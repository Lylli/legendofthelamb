package be.dastudios.legendofthelamb.monsters;

import be.dastudios.legendofthelamb.player.utility.abilities.Dice;
import be.dastudios.legendofthelamb.player.backpack.items.miscItems.Item;

import java.util.ArrayList;
import java.util.List;

public class Wolf extends Monster {
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RESET = "\u001B[0m";
    private Dice dice;
    private int attackChance;
    private int damage;
    private List<Item> items = new ArrayList<>();

    public Wolf(){
        setName("Wolf");
        setArmourClass(19);
        setHp(40);
        setCr(0.5);
    }

    public void seeInventory(){
        items.forEach(System.out::println);
    }

    public int attack() {
        dice = new Dice();
        int totalAttack = dice.rollD20() + 3;
        System.out.println(ANSI_GREEN + " Wolf " + ANSI_RESET + " is trying to attack!");
        return totalAttack;
    }

    public int damage(){
        dice = new Dice();
        int totalDamage = dice.rollD6() + 3;
        System.out.println(ANSI_GREEN + " Wolf" + ANSI_RESET + " bites, better watch out for the next full moon!");
        return totalDamage;
    }


    @Override
    public String toString() {
        return "You hear rustling behind you" +"\n"+
                "You see a wolf staring at you while growling!" +"\n"+
                "------------------------------------" + "\n"+
                "You are encountering a wolf! Stats: " + "\n" +
                "ArmourClass: " + getArmourClass() +", HP: " + getHP() +
                ", CR: " + getCr();
    }
}
