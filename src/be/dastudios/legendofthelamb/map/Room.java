package be.dastudios.legendofthelamb.map;

import be.dastudios.legendofthelamb.Game;

import java.io.Serializable;

/**
 * The player will start in the startRoom and can choose to go to other maps. So for example: Room1= startroom, room2=forestofstreams
 * if player goes to room2, a tilegrid is created and forestofstreams is a whole map (kinda like loading screen dungeons).
 * So a room consists of a name, a tilegrid, direction and description.
 */
public class Room implements Serializable {
    private String name;
    private String description;
    private int n,s,w,e;

    public Room(String name, String description, int n, int s, int w, int e){
        this.name= name;
        this.description = description;
        this.n =n;
        this.s=s;
        this.w=w;
        this.e=e;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public int getS() {
        return s;
    }

    public void setS(int s) {
        this.s = s;
    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public int getE() {
        return e;
    }

    public void setE(int e) {
        this.e = e;
    }
}
