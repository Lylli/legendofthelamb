package be.dastudios.legendofthelamb.player.race;

public class Dwarf extends Race {
    private final String name = "Dwarf";
    private final int baseSpeed = 5;
    private final int strength = 2;
    private final int constitution = 2;
    private String description = "Dwarves are masters of mountains and hills. They live mostly underground and are quite grumpy by nature.\n" +
            " But once you have the trust of a dwarf, they do everything for you, albeit with a grumble and a snort.";

    public Dwarf(){
    }
    public String getName(){
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getBaseSpeed() {
        return baseSpeed;
    }

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public int getConstitution() {
        return constitution;
    }

}
