package be.dastudios.legendofthelamb.monsters;

import be.dastudios.legendofthelamb.player.utility.abilities.Dice;
import be.dastudios.legendofthelamb.player.backpack.items.armor.LeatherArmour;
import be.dastudios.legendofthelamb.player.backpack.items.miscItems.Item;
import be.dastudios.legendofthelamb.player.backpack.items.weapons.Sword;

import java.util.ArrayList;
import java.util.List;

public class Hobgoblin extends Monster {
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RESET = "\u001B[0m";
    private Dice dice;
    private int attackChance;
    private int damage;
    private List<Item> items = new ArrayList<>();


    public Hobgoblin(){
        setName("Hobgoblin");
        setArmourClass(18);
        setHp(25);
        setCr(2);
        Sword sword = new Sword();
        LeatherArmour leatherArmour = new LeatherArmour();
        items.add(sword);
        items.add(leatherArmour);
        setWallet(5);
    }

    public void seeInventory(){
        items.forEach(System.out::println);
    }

    public int attack() {
        dice = new Dice();
        int totalAttack = dice.rollD20() + 4;
        System.out.println(ANSI_GREEN + " Hobgoblin " + ANSI_RESET + " is trying to attack!");
        return totalAttack;
    }

    public int damage(){
        dice = new Dice();
        int totalDamage = dice.rollD8() + 3;
        System.out.println(ANSI_GREEN + " Hobgoblin " + ANSI_RESET + " hits with the club!\n" +
                "You say : 'Alright, now I believe you can lift the club!'");
        return totalDamage;
    }
    @Override
    public String toString() {
        return "You see a large goblinoid with dark orange or red-orange skin, dragging a club behind him (or is it a her?)" +"\n"+
                "You say to the monster: 'That club looks heavy! Are you sure you can lift that?'"+"\n"+
                "------------------------------------" + "\n"+
                "You are encountering a Hobgoblin! Stats: " + "\n" +
                "ArmourClass: " + getArmourClass() +", HP: " + getHP() +
                ", CR: " + getCr();
    }
}
