package be.dastudios.legendofthelamb.player.gameclass;

import be.dastudios.legendofthelamb.combatSystem.Combat;
import be.dastudios.legendofthelamb.monsters.Monster;
import be.dastudios.legendofthelamb.player.backpack.BackPack;
import be.dastudios.legendofthelamb.player.utility.abilities.AbilityModifier;
import be.dastudios.legendofthelamb.player.utility.abilities.Dice;
import be.dastudios.legendofthelamb.player.utility.abilities.AbilityCalculator;
import be.dastudios.legendofthelamb.player.backpack.ClericalBackPack;
import be.dastudios.legendofthelamb.player.backpack.items.armor.Clothes;
import be.dastudios.legendofthelamb.player.backpack.items.weapons.Dagger;
import be.dastudios.legendofthelamb.player.backpack.items.weapons.HolySymbol;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.Keyboard;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.LayOutText;

import java.util.ArrayList;

public class Healer extends GameClass{
        private final String nameClass = "Healer";
        private int lifeDiceOutcome;
        private int armourBase = 4;
        private int choiceAttackLevel5;

        private int strength;
        private int constitution;
        private int wisdom;
        private int charisma;
        private int dexterity;
        private int intelligence;

        private ClericalBackPack clericalBackPack = new ClericalBackPack();
        private int wallet;

        public Healer (){
            this.lifeDiceOutcome = getHP();
            this.clericalBackPack = startersClericBackpack();
        }

    public ClericalBackPack startersClericBackpack(){
        Dagger dagger = new Dagger();
        HolySymbol holySymbol = new HolySymbol();
        Clothes clothes = new Clothes();
        this.armourBase += clothes.getArmourBaseClothes();
        this.clericalBackPack.addItemInBackPack(dagger);
        this.clericalBackPack.addItemInBackPack(holySymbol);
        this.clericalBackPack.addItemInBackPack(clothes);
        this.wallet = 15;
        this.clericalBackPack.addGpInBackPack(wallet);
        return clericalBackPack;
    }

    @Override
    public String getNameClass() {
        return nameClass;
    }

    public ClericalBackPack getBackpack() {
        return clericalBackPack;
    }

    public int getHP(){
            int total = 0;
            for (int k = 0; k<3; k++){
                Dice lifeDice = new Dice();
                total += lifeDice.rollD6();
            }
            return total +10;
        }

        public void setAbilityValuesStandard(){
            wisdom = 16 + 2;
            charisma = 14 + 1;
            constitution = 14;
            intelligence = 13;
            dexterity = 11;
            strength = 8;
        }

        public void setAbilityValuesVariable() {
            AbilityCalculator ac = new AbilityCalculator();
            ArrayList<Integer> abilityValues = ac.finalVariableValues();
            wisdom = abilityValues.get(0) + 2;
            charisma = abilityValues.get(1) + 1;
            constitution = abilityValues.get(2);
            intelligence = abilityValues.get(3);
            dexterity = abilityValues.get(4);
            strength = abilityValues.get(5);
        }

    public int getArmourBase(){
            return armourBase;
        }

    public int getLifeDiceOutcome() {
        return lifeDiceOutcome;
    }

    public void setLifeDiceOutcome(int lifeDiceOutcome) {
        this.lifeDiceOutcome = lifeDiceOutcome;
    }

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public void setStrength(int strength) {
        this.strength = strength;
    }

    @Override
    public int getConstitution() {
        return constitution;
    }

    @Override
    public void setConstitution(int constitution) {
        this.constitution = constitution;
    }

    @Override
    public int getWisdom() {
        return wisdom;
    }

    @Override
    public void setWisdom(int wisdom) {
        this.wisdom = wisdom;
    }

    @Override
    public int getCharisma() {
        return charisma;
    }

    @Override
    public void setCharisma(int charisma) {
        this.charisma = charisma;
    }

    @Override
    public int getDexterity() {
        return dexterity;
    }

    @Override
    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    @Override
    public int getIntelligence() {
        return intelligence;
    }

    @Override
    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public void setArmourBase(int armourBase) {
        this.armourBase = armourBase;
    }

    @Override
    public BackPack getClericalBackpack(){
            return clericalBackPack;
    }

    @Override
    public int attack() {
        int totalAttack = 0;
        Keyboard keyboard = new Keyboard();
        switch (getLevel()){
            case 1:
            case 2:
            case 3:
            case 4:
                totalAttack = curseAttack();
                break;
            case 5:
                this.choiceAttackLevel5 = keyboard.readInt("Please choose your attack: \n" +
                        "1. Cursing word\n" +
                        "2. Maddening Prayer\n" +
                        "3. God's area\n" );
                while (choiceAttackLevel5 < 1 || choiceAttackLevel5 > 3){
                    System.out.println("Please choose a valid number.\n");
                    this.choiceAttackLevel5 = keyboard.readInt("Please choose your attack: \n" +
                            "1. Cursing word\n" +
                            "2. Maddening Prayer\n" +
                            "3. God's area\n" );
                }
                if (choiceAttackLevel5 != 3){
                    totalAttack = curseAttack();
                } else {
                    Dice dice = new Dice();
                    int outcomeDice = dice.rollD6();
//                    if (outcomeDice >= 5) {
                        totalAttack = 10000000;
//                    } else {
//                        totalAttack = 0;
//                    }
                    break;
                }
        } return totalAttack;
    }


    public int heal(){
        int totalHeal = 0;
        Dice dice = new Dice();
        Keyboard keyboard = new Keyboard();
        AbilityModifier am = new AbilityModifier(getWisdom());
            switch (getLevel()){
                case 1:
                case 2:
                    totalHeal = dice.rollD6() + am.useModifier();
                    break;
                case 3:
                case 4:
                case 5:
                    int healChoice = keyboard.readInt("Please choose your heal: \n" +
                            "1. Healing Word\n" +
                            "2. Healing Prayer\n");
                    while (healChoice < 1 || healChoice> 2){
                        System.out.println("Please choose a valid number.\n");
                        healChoice = keyboard.readInt("Please choose your heal: \n" +
                                "1. Healing Word\n" +
                                "2. Healing Prayer\n");
                    }
                    if (healChoice == 1){
                        totalHeal = dice.rollD6() + am.useModifier();
                    } else {
                        totalHeal = dice.rollD12() + am.useModifier();
                    }
                    break;
            }return totalHeal;
        }
    private int godsAreaHeal(){
            Dice dice = new Dice ();
            Dice dice1 = new Dice();
            AbilityModifier am = new AbilityModifier(getWisdom());
            int totalHeal = dice.rollD12() + dice1.rollD12() + am.useModifier();
            return totalHeal;
    }
    @Override
    public int damage(Monster monster) {
        Keyboard keyboard = new Keyboard();
        Dice dice = new Dice();
        AbilityModifier abilityModifier = new AbilityModifier(getWisdom());
        int totalDamage = 0;
        switch (getLevel()){
            case 1:
            case 2:
            case 3:
//                TODO creature can't attack for one turn bij Cursing word
                int levelDamage = 0;
                do {
                    levelDamage = dice.rollD6() + abilityModifier.useModifier();
                } while (levelDamage == 0);
                int holySymbol = dice.rollD6() + getWisdom();
                totalDamage = holySymbol / levelDamage;
                System.out.println("You cast Cursing Word.");
                break;
            case 4:
                int curseChoice = keyboard.readInt("Please choose your curse: \n" +
                        "1. Cursing word\n" +
                        "2. Maddening Prayer\n");
                while (curseChoice < 1 || curseChoice > 2){
                    System.out.println("Please choose a valid number.\n");
                    curseChoice = keyboard.readInt("Please choose your curse: \n" +
                            "1. Cursing word\n" +
                            "2. Maddening Prayer\n");
                }
                if (curseChoice == 1){
                    levelDamage = 0;
                    do {
                        levelDamage = dice.rollD6() + abilityModifier.useModifier();
                    } while (levelDamage == 0);
                    holySymbol = dice.rollD6() + getWisdom();
                    totalDamage = holySymbol / levelDamage;
                    System.out.println("You cast Cursing Word.");

                } else {
                    System.out.println("You cast Maddening Prayer.");
                    totalDamage = monster.damage();

                }
                break;
            case 5:
                if(choiceAttackLevel5 == 3) {
                   godsAreaHeal();
                    Dice dice1 = new Dice();
                    AbilityModifier am = new AbilityModifier(getWisdom());
                    totalDamage = dice.rollD12() + dice1.rollD12() + am.useModifier();
                    System.out.println("Letla has little weather, but it certainly has lightning.\n");
                    try {
                        Thread.sleep(2000);
                        System.out.println("Clouds are gathering nearby.\n");
                        Thread.sleep(2000);
                        System.out.println("Flashes of light flicker. When this is over, there will be nobody left to mourn.");
                        Thread.sleep(2000);
                        LayOutText.prettyLines();
                    } catch (InterruptedException ie) {
                    }

                } else if (choiceAttackLevel5 == 1){
                    levelDamage = 0;
                    do {
                        levelDamage = dice.rollD6() + abilityModifier.useModifier();
                    } while (levelDamage == 0);
                    holySymbol = dice.rollD6() + getWisdom();
                    totalDamage = holySymbol / levelDamage;
                    System.out.println("You cast Cursing Word.");
                    return totalDamage;

                } else {
                    System.out.println("You cast Maddening Prayer.");
                    totalDamage = monster.damage();

                }
                break;


        } return totalDamage;
    }


    private int curseAttack() {
        int totalAttack;
        Dice dice = new Dice();
        AbilityModifier abilityModifier = new AbilityModifier(getWisdom());
        int levelAttack = dice.rollD20() + getProficiency() + abilityModifier.useModifier();
        int holySymbol = dice.rollD20() + getProficiency() + getWisdom();
        totalAttack = (holySymbol + levelAttack)/2;
        return totalAttack;
    }



    @Override
    public int getWeaponChoice() {
        return 0;
    }

    @Override
    public void setWeaponChoice() {

    }

    @Override
        public String toString() {
            return "Healer{" +
                    "nameClass='" + nameClass + '\'' +
                    ", lifeDiceOutcome=" + lifeDiceOutcome +
                    ", strength=" + strength +
                    ", constitution=" + constitution +
                    ", wisdom=" + wisdom +
                    ", charisma=" + charisma +
                    ", dexterity=" + dexterity +
                    ", intelligence=" + intelligence +
                    '}';
        }
}


