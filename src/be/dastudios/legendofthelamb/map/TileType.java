package be.dastudios.legendofthelamb.map;

public enum TileType {
    Trees("trees"), Path ("a path"), River ("a river"), Intersection ("an intersection)");

    String textureName;


    TileType(String textureName){
        this.textureName = textureName;
    }

    public String getTextureName() {
        return textureName;
    }

    public void setTextureName(String textureName) {
        this.textureName = textureName;
    }

//    @Override
//    public String toString() {
//        return  "you see " + textureName;
//    }
}
