package be.dastudios.legendofthelamb.monsters;

import be.dastudios.legendofthelamb.Game;
import be.dastudios.legendofthelamb.player.utility.abilities.Dice;

import java.io.Serializable;

/**
 * This is an abstract class for all the monsters in the game, because each monster has certain fields and methods.
 * This class enables us to make monsters which will be defined in their own subclass.
 * @see be.dastudios.legendofthelamb.combatSystem.Combat
 */
public abstract class Monster implements Serializable {
    private String name;
    private int armourClass;
    private int hp;
    private double cr;
    private int initiative;
    private int wallet;

    public int getWallet() {
        return wallet;
    }

    public void setWallet(int wallet) {
        this.wallet = wallet;
    }

    public int rollInitiative(){
        Dice dice = new Dice();
        this.initiative = dice.rollD20();
        return initiative;
    }

    public int attack(){
        return 0;
    }
    public int damage(){
        return 0;
    }
    public int getArmourClass(){
        return armourClass;
    }
    public int getHP(){
        return hp;
    }

    public void setHp (int hp){
        this.hp = hp;
    }

    public void setArmourClass(int armourClass) {
        this.armourClass = armourClass;
    }

    public int getHp() {
        return hp;
    }

    public double getCr() {
        return cr;
    }

    public void setCr(double cr) {
        this.cr = cr;
    }

    public int getInitiative() {
        return initiative;
    }

    public void setInitiative(int initiative) {
        this.initiative = initiative;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
