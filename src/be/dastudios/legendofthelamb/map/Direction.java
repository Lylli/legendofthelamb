package be.dastudios.legendofthelamb.map;

import be.dastudios.legendofthelamb.Game;

public enum Direction {
    NORTH,
    SOUTH,
    EAST,
    WEST;

    public static final int NOEXIT= -1;
}
