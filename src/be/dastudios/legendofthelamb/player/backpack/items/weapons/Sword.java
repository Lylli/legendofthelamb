package be.dastudios.legendofthelamb.player.backpack.items.weapons;


import be.dastudios.legendofthelamb.player.backpack.items.miscItems.Item;

public class Sword extends Item {
    private String name = "Sword";
    private String description = "A sharpened piece of steel with a little wooden handle. Lethal how ever you look at it";

    public Sword(){
        this.name = "Sword";
    }

    @Override
    public String toString() {
        return " " + name + " - description: " + description;
    }



}
