package be.dastudios.legendofthelamb.player.backpack.items.weapons;

import be.dastudios.legendofthelamb.player.backpack.items.miscItems.Item;

public class Dagger extends Item {
    private String name = "Dagger";
    private String description = "A small knife that can be used in different situations";

    public Dagger(){
        this.name = "Dagger";
    }

    @Override
    public String toString() {
        return " " + name + " - description: " + description;
    }



}
