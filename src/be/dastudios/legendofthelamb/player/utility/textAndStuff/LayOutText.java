package be.dastudios.legendofthelamb.player.utility.textAndStuff;

import be.dastudios.legendofthelamb.Game;

import java.io.Serializable;

/**
 * This is a class to format our text in the game.
 * It also has some standard phrases that are used more frequently during the game.
 */
public class LayOutText implements Serializable {

    public static void prettyLines(){
        System.out.println( "------------------------------------\n");
    }
    public static void notAnOption(){
        System.out.println("This option isn't available...." + "\n" + "\n" + "muhahahaha...");
        prettyLines();
    }
    public static String pressAnyKeyToContinue(){
        return "Press any key to continue";
    }

    public static String mainMenu(){
        String mainMenuText = "What do you want to do?" + "\n" +
        "New\t\t\t - Start a new game\n" +
                "Load\t\t - Load a saved game\n" +
                "Delete\t\t - Delete a saved game\n" +
                "Controls\t - Game controls\n" +
                "Settings\t - Game settings\n" +
                "Quit\t\t - Quit game";
        return mainMenuText;
    }
}
