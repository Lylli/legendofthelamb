package be.dastudios.legendofthelamb.player.utility.abilities;

import be.dastudios.legendofthelamb.Game;
import be.dastudios.legendofthelamb.player.Player;

import java.io.Serializable;

/**
 * This is a class to calculate the ability modifiers that are being used during playercreation and combat.
 * @see Player
 */
public class AbilityModifier implements Serializable {
    int modifier;

    public AbilityModifier(int modifier){
        this.modifier = modifier;
    }

    public int useModifier(){
        this.modifier = (modifier-10)/2;
        return modifier;
    }
}
