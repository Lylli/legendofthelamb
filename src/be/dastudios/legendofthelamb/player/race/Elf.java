package be.dastudios.legendofthelamb.player.race;

public class Elf extends Race {
    private final String name = "Elf";
    private final int baseSpeed = 7;
    private final int dexterity = 2;
    private final int intelligence = 2;
    private String description = "Elves are majestic beings who live in the higher spheres of longevity. An elf can get up to 2000 years before\n" +
            "perishing of this world. This makes them look down upon humans as these races struggle with meaningless wars\n" +
            "that last only a couple of years.";

    @Override
    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getBaseSpeed(){
        return baseSpeed;
    }

    public int getDexterity(){
        return dexterity;
    }

    public int getIntelligence(){
        return intelligence;
    }

}
