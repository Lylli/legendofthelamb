package be.dastudios.legendofthelamb.player.backpack;

import be.dastudios.legendofthelamb.player.backpack.items.miscItems.GoldPiece;
import be.dastudios.legendofthelamb.player.backpack.items.miscItems.Item;
import be.dastudios.legendofthelamb.player.backpack.items.miscItems.Spellbook;

import java.util.ArrayList;
import java.util.List;
/**
 * This is the backpack that can only be used by healers or spellcasters.
 * You can hold regular items in it, gold pieces and there is a seperate sachet for spellbooks called pack of Knowledge.
 * Spellcasters are not yet in the game.
 */
public class ClericalBackPack extends BackPack {
    private final int MAX_ITEMS = 15;
    private final int MAX_GP = 300;
    private String name = "Clerical backpack";
    private String description = "You tank, I'll heal you.";
    private final int MAX_SPELLBOOKS = 5;
    List<Item> backpack = new ArrayList<>();
    int wallet;
    List <Spellbook> packOfKnowledge = new ArrayList<>();
    Spellbook spellbook;
    Item item;
    int gp;

    /**
     * Spellbooks are one time cast spells only and can be found throughout the map of the game
     * or as loot of monsters.
     * Spellbooks and loot aren't created yet.
     */
    public void addSpellbookInPackOfKnowledge(Spellbook sb){
        if (packOfKnowledge.size() < MAX_SPELLBOOKS){
            packOfKnowledge.add(sb);
            System.out.println("Sure, like I wasn't heavy enough already!");
        }
        else {
            System.out.println("If you're so smart, why aren't you rich? My pack of knowledge is full");
        }
    }

    public void addItemInBackPack(Item item){
        if (backpack.size() <= MAX_ITEMS){
            backpack.add(item);
        }
        else {
            System.out.println("I am sworn to carry your burdens. However I can't carry anymore.");
        }
    }

    public void addGpInBackPack(int gp){
        if (wallet < MAX_GP) {
            if (gp > MAX_GP) {
                System.out.println("The reward is too big. LOL!"+"\n"+
                        "Retracting till total is valid.");
                this.wallet = MAX_GP;
            } else {
                this.wallet += gp;
            }
        }
        else {
            System.out.println("You are too rich, go spend some gold pieces.");
        }
    }



    @Override
    public void printWallet() {
        System.out.println("You have now " + this.wallet + " gold coins!");
    }

    public List<Item> seeInventory(){
        return backpack;
    }

    public int seeTotalCoins(){
        return wallet;
    }

    @Override
    public void seePackOfKnowledge(){
        System.out.println("Pack of knowledge contains: ");
        packOfKnowledge.forEach(System.out::println);
    }

    public void printBackPack(){
        System.out.println(name + "\n" +
                "(Oh dear, it talks):" + "\n" +
                description + "\n" +
                "Contents: ");
        backpack.forEach(System.out::println);
        packOfKnowledge.forEach(System.out::println);
        System.out.println("Gold: " + wallet + " gp");
    }

    @Override
    public String toString() {
        return "" + name + " (oh dear, it talks):" + "\n" +
                "" + description + "\n" +
                "Contents " + backpack + "\n" +
                "Pack of Knowledge: " + packOfKnowledge + "\n" +
                "Gold: " + wallet + " gp";
    }
}
