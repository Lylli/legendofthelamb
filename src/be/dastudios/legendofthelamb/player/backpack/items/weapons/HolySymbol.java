package be.dastudios.legendofthelamb.player.backpack.items.weapons;

import be.dastudios.legendofthelamb.player.backpack.items.miscItems.Item;

public class HolySymbol extends Item {
    private String name = "Holy Symbol";
    private String description = "An emblem to a god that is imbued with its power. Can be used to cast various spells that need support off the\n" +
            "Wisdom Ability";

    public HolySymbol(){
        this.name = "Holy Symbol";
    }

    @Override
    public String toString() {
        return " " + name + " - description: " + description;
    }

}
