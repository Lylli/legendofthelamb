package be.dastudios.legendofthelamb.player.utility.abilities;

import be.dastudios.legendofthelamb.Game;

import java.io.Serializable;
import java.util.Random;
/**
 * This is a class to roll dice when it's needed during the game.
 * Basicly a RNG simulator.
 */
public class Dice implements Serializable {
    Random random = new Random();
    private int d2 = random.nextInt(2) +1;
    private int d4 = random.nextInt(4) +1;
    private int d6 = random.nextInt(6) +1;
    private int d8 = random.nextInt(8) +1;
    private int d10 = random.nextInt(10) +1;
    private int d12 = random.nextInt(12) +1;
    private int d20 = random.nextInt(20) +1;
    private int d100 = random.nextInt(100) +1;


    public Dice(){
    }
   public int rollD2(){

       return d2;
   }
    public int rollD4(){

        return d4;
    }
    public int rollD6(){

        return d6;
    }
    public int rollD8(){

        return d8;
    }
    public int rollD10(){

        return d10;
    }
    public int rollD12(){
        return d12;
    }
    public int rollD20(){
        return d20;
    }
    public int rollD100(){
        return d100;
    }
}
