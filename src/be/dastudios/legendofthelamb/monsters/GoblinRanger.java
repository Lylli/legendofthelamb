package be.dastudios.legendofthelamb.monsters;

import be.dastudios.legendofthelamb.player.utility.abilities.Dice;
import be.dastudios.legendofthelamb.player.backpack.items.armor.LeatherArmour;
import be.dastudios.legendofthelamb.player.backpack.items.miscItems.Item;
import be.dastudios.legendofthelamb.player.backpack.items.weapons.Bow;

import java.util.ArrayList;
import java.util.List;

public class GoblinRanger extends Monster {
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RESET = "\u001B[0m";
    private Dice dice;
    private int attackChance;
    private int damage;
    private List<Item> items = new ArrayList<>();

    public GoblinRanger(){
        setName("Goblin Ranger");
        setArmourClass(15);
        setHp(12);
        setCr(1);
        Bow bow = new Bow();
        LeatherArmour leatherArmour = new LeatherArmour();
        items.add(bow);
        items.add(leatherArmour);
        setWallet(5);
    }

    public void seeInventory(){
        items.forEach(System.out::println);
    }

    public int attack() {
        dice = new Dice();
        int totalAttack = dice.rollD20() + 4;
        System.out.println(ANSI_GREEN + " Goblin Ranger " + ANSI_RESET + " is trying to attack!");
        return totalAttack;
    }

    public int damage(){
        dice = new Dice();
        int totalDamage = dice.rollD6() + 3;
        System.out.println(ANSI_GREEN + " Goblin Ranger " + ANSI_RESET + " shoots an arrow. Hopefully not at someone's knee.");
        return totalDamage;
    }

    @Override
    public String toString() {
        return "You hear rustling behind you" +"\n"+
                "Is that an arrow?" +"\n"+
                "------------------------------------" + "\n"+
                "You are encountering a goblin ranger! Stats: " + "\n" +
                "ArmourClass: " + getArmourClass() +", HP: " + getHP() +
                ", CR: " + getCr();
    }
}
