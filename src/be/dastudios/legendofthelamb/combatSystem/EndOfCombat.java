package be.dastudios.legendofthelamb.combatSystem;

import be.dastudios.legendofthelamb.Game;
import be.dastudios.legendofthelamb.monsters.Monster;
import be.dastudios.legendofthelamb.player.Player;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.LayOutText;

import java.io.Serializable;

public class EndOfCombat implements Serializable {
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RESET = "\u001B[0m";

    public void deathCreature(Monster monster, Player player) {
        if (monster.getHP() <= 0) {
            System.out.println("" + ANSI_GREEN + monster.getName() + ANSI_RESET + " has been defeated! Go you!");
            LayOutText.prettyLines();
            System.out.println("You have " + player.getHp() + " HP left.");
            LayOutText.prettyLines();
            getXP(player,monster);
            stealGold(player, monster);
        }
        if (player.getHp() <= 0) {
            System.out.println("You have DIED!" + "\n" +
                    "█████████████████\n" +
                    "█▄─▄▄▀█▄─▄█▄─▄▄─█\n" +
                    "██─▄─▄██─███─▄▄▄█\n" +
                    "█▄▄█▄▄█▄▄▄█▄▄▄███");
        }
        LayOutText.prettyLines();
    }

    // TODO getXP en checkCR kunnen in een enumeratie

    private void getXP(Player player, Monster monster) {
        double monsterCR = monster.getCr();
        checkCR(player, monsterCR, 0, 10, player.getExp());
        checkCR(player, monsterCR, 0.25, 25, player.getExp());
        checkCR(player, monsterCR, 0.5, 50, player.getExp());
        checkCR(player, monsterCR, 1, 100, player.getExp());
        checkCR(player, monsterCR, 2, 150, player.getExp());
        checkCR(player, monsterCR, 3, 450, player.getExp());
        checkCR(player, monsterCR, 4, 800, player.getExp());
        checkCR(player, monsterCR, 5, 2000, player.getExp());

    }

    private void checkCR(Player player, double monsterCR, double cr, int xp, int exp) {
        if (monsterCR == cr) {
            int x = player.getExp();
            player.setExp(player.getExp() + xp);
            System.out.println("Congratulations! You gained " + xp + "XP.\n" +
                    "You have now " + player.getExp() + " XP.");
            LayOutText.prettyLines();
            levelUp(player, x, 100, 2);
            levelUp(player, x, 250, 3);
            levelUp(player, x, 700, 4);
            levelUp(player, x, 1500, 5);
        }
    }

    private void levelUp(Player player, int x, int levelRange, int level) {
        if (x < levelRange && player.getExp() >= levelRange) {
            player.getGameClass().setLevel(level);
            System.out.println("Tatatataaaaaa! You leveled up! You are now level " +  level + "!");
            LayOutText.prettyLines();
        }
    }
    private void stealGold(Player player, Monster monster) {
        System.out.println("You steal the wallet of the " + ANSI_GREEN + monster.getName() + ANSI_RESET + " which contains " + monster.getWallet() + " gold coins!");
        player.getBackpack().addGpInBackPack(monster.getWallet());
        player.getBackpack().printWallet();
        LayOutText.prettyLines();
    }
}
