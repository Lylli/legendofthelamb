package be.dastudios.legendofthelamb.monsters;

import be.dastudios.legendofthelamb.player.utility.abilities.Dice;
import be.dastudios.legendofthelamb.player.backpack.items.armor.LeatherArmour;
import be.dastudios.legendofthelamb.player.backpack.items.miscItems.Item;
import be.dastudios.legendofthelamb.player.backpack.items.weapons.Sword;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Troll extends Monster {
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RESET = "\u001B[0m";
    private Dice dice;
    private Dice dice1;
    private int randomAttack;
    private int attackChance;
    private int damage;
    private List<Item> items = new ArrayList<>();
    private Random random = new Random();

    public Troll(){
        setName("Troll");
        setArmourClass(19);
        setHp(40);
        setCr(4);
        Sword sword = new Sword();
        LeatherArmour leatherArmour = new LeatherArmour();
        items.add(sword);
        items.add(leatherArmour);
        setWallet(5);
    }

    public void seeInventory(){
        items.forEach(System.out::println);
    }

    public void setRandomAttack(int randomAttack){
        this.randomAttack = randomAttack;
    }

    public int getRandomAttack(){
        return randomAttack;
    }

    public int attack() {
        setRandomAttack(random.nextInt(2));
        int totalAttack = 0;
        if (getRandomAttack()==0){
            dice = new Dice();
            totalAttack = dice.rollD20() + 5;
            System.out.println(ANSI_GREEN + " Troll " + ANSI_RESET + " is trying to attack!");
        } else {
            dice = new Dice();
            dice1 = new Dice();
            totalAttack = ((dice.rollD20() + dice1.rollD20())/ 2) + 7;
            System.out.println(ANSI_GREEN + " Troll " + ANSI_RESET + " is trying to attack!");
        }
        return totalAttack;
    }

    public int damage() {
        int totalDamage = 0;
        if (getRandomAttack() == 0) {
            dice = new Dice();
            totalDamage = dice.rollD10() + 4;
            System.out.println(ANSI_GREEN + " Troll " + ANSI_RESET + " hits with the BIG club!");
        } else {
            dice = new Dice();
            totalDamage = (dice.rollD10() * 2) + 4;
            System.out.println(ANSI_GREEN + " Troll " + ANSI_RESET + " hits with the BIG club! It was a heavy blow!");
        }
        return totalDamage;
    }

        @Override
        public String toString() {
            return "You see a large, bipedal green-skinned giant that is about one and a half times as tall as a human but very thin. It has long and ungainly arms and legs." + "\n" +
                    "The legs end in great three-toed feet, the arms in wide, powerful hands with sharpened claws holding a mighty club." +"\n"+
                    "You say to the monster: 'That is a BIG club!'"+"\n"+
                    "------------------------------------" + "\n"+
                    "You are encountering a Troll! Stats: " + "\n" +
                    "ArmourClass: " + getArmourClass() +", HP: " + getHP() +
                    ", CR: " + getCr();
        }
}
