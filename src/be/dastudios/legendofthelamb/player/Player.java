package be.dastudios.legendofthelamb.player;

import be.dastudios.legendofthelamb.combatSystem.Combat;
import be.dastudios.legendofthelamb.player.utility.abilities.AbilityModifier;
import be.dastudios.legendofthelamb.player.utility.abilities.Dice;
import be.dastudios.legendofthelamb.player.backpack.BackPack;
import be.dastudios.legendofthelamb.player.gameclass.Fighter;
import be.dastudios.legendofthelamb.player.gameclass.GameClass;
import be.dastudios.legendofthelamb.player.gameclass.Healer;
import be.dastudios.legendofthelamb.player.gameclass.Ranger;
import be.dastudios.legendofthelamb.player.race.Dwarf;
import be.dastudios.legendofthelamb.player.race.Elf;
import be.dastudios.legendofthelamb.player.race.Human;
import be.dastudios.legendofthelamb.player.race.Race;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.Keyboard;

import java.util.Scanner;

public class Player extends Combat {
    private String name;
    private Race race;
    private GameClass gameClass;

    int level = 1;
    int exp = 0;
    int proficiency = level/2;

    int initiative;
    int armourClass;
    private int weaponChoice;

    private int hp;
    private int hPOfCreatedPlayer;
    private int strength;
    private int constitution;
    private int wisdom;
    private int charisma;
    private int dexterity;
    private int intelligence;

    private AbilityModifier abilityModifier;
    private int highestValue;

    private BackPack backpack;

    public Player (){
    }

    public Player(String name){
        this.name = name;
        setRace();
        askAndSetGameClass();


    }

    public int getProficiency() {
        return proficiency = level/2;
    }

    public int getStrength() {
        return strength;
    }

    public int getConstitution() {
        return constitution;
    }

    public int getWisdom() {
        return wisdom;
    }

    public int getCharisma() {
        return charisma;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    private void setRace() {
        Keyboard keyboard = new Keyboard();
        int choice = keyboard.readInt("Choose your Race:\n" +
                "1. Human\n" +
                "2. Elf\n" +
                "3. Dwarf");
        while(choice < 1 || choice>3){
            System.out.println("Please enter a valid number.");
            choice = keyboard.readInt("Choose your Race:\n" +
                    "1. Human\n" +
                    "2. Elf\n" +
                    "3. Dwarf");
        }
        switch (choice){
            case 1:
                Human human = new Human();
                setAbilitiesRace(human);
                this.race = human;
                break;
            case 2:
                Elf elf = new Elf();
                setAbilitiesRace(elf);
                this.race = elf;
                break;
            case 3:
            Dwarf dwarf = new Dwarf();
            setAbilitiesRace(dwarf);
            this.race = dwarf;
            break;
        }
    }
    public Race getRace(){
        return race;
    }

    private void askAndSetGameClass(){
        Keyboard keyboard = new Keyboard();
        int choice = keyboard.readInt("Choose your class:\n" +
                "1. Fighter\n" +
                "2. Ranger\n" +
                "3. Healer");
        while(choice < 1 || choice > 3) {
            System.out.println("Please enter a valid number.");
            choice = keyboard.readInt("Choose your class:\n" +
                    "1. Fighter\n" +
                    "2. Ranger\n" +
                    "3. Healer");
        }
        System.out.println();
        int abilityChoice = keyboard.readInt("Would you like to assign the standard ability(1) scores or the variable ability(2) scores?\n");
        while(abilityChoice < 1 || abilityChoice > 2) {
            System.out.println("Please enter a valid number.");
            abilityChoice = keyboard.readInt("Would you like to assign the standard ability(1) scores or the variable ability(2) scores?\n");
        }
        switch (choice) {
            case 1:
                Fighter fighter = new Fighter();
                this.gameClass = fighter;
                this.hp = fighter.getLifeDiceOutcome();
                this.hPOfCreatedPlayer = hp;
                if (abilityChoice == 1){
                    fighter.setAbilityValuesStandard();
                    setAbilitiesGameClass(fighter);
                    abilityModifier = new AbilityModifier(getStrength());
                    calculateArmourClass();
                } else {
                    fighter.setAbilityValuesVariable();
                    setAbilitiesGameClass(fighter);
                    abilityModifier = new AbilityModifier(getStrength());
                    calculateArmourClass();
                }
                this.backpack = fighter.getBackpack();
                break;
            case 2:
                Ranger ranger = new Ranger();
                this.gameClass = ranger;
                this.hp = ranger.getLifeDiceOutcome();
                this.hPOfCreatedPlayer = hp;
                if (abilityChoice == 1){
                    ranger.setAbilityValuesStandard();
                    setAbilitiesGameClass(ranger);
                    abilityModifier = new AbilityModifier(getHighestAbilityValue());
                    calculateArmourClass();

                }else {
                    ranger.setAbilityValuesVariable();
                    setAbilitiesGameClass(ranger);
                    abilityModifier = new AbilityModifier(getHighestAbilityValue());
                    calculateArmourClass();
                }
                this.backpack = ranger.getBackpack();
                break;
            case 3:
                Healer healer = new Healer();
                this.gameClass = healer;
                this.hp = healer.getLifeDiceOutcome();
                this.hPOfCreatedPlayer = hp;
                if (abilityChoice == 1){
                    healer.setAbilityValuesStandard();
                    setAbilitiesGameClass(healer);
                    abilityModifier = new AbilityModifier(getDexterity());
                    calculateArmourClass();
                } else {
                    healer.setAbilityValuesVariable();
                    setAbilitiesGameClass(healer);
                    abilityModifier = new AbilityModifier(getDexterity());
                    calculateArmourClass();
                }
                this.backpack = healer.getBackpack();
                break;
        }
    }

    public GameClass getGameClass() {
        return gameClass;
    }

    public int getHighestAbilityValue(){
        if (getStrength() > getDexterity()){
            return highestValue = getStrength();
        } else {
            return highestValue = getDexterity();
        }
    }

    public void calculateArmourClass(){
       this.armourClass = getGameClass().getArmourBase() + abilityModifier.useModifier();
    }

    public void setAbilitiesRace(Race race){
        this.strength += race.getStrength();
        this.constitution += race.getConstitution();
        this.wisdom += race.getWisdom();
        this.charisma += race.getCharisma();
        this.dexterity += race.getDexterity();
        this.intelligence += race.getIntelligence();
    }

    public void setAbilitiesGameClass(GameClass gameClass){
        this.strength += gameClass.getStrength();
        this.constitution += gameClass.getConstitution();
        this.wisdom += gameClass.getWisdom();
        this.charisma += gameClass.getCharisma();
        this.dexterity += gameClass.getDexterity();
        this.intelligence += gameClass.getIntelligence();
    }

    public int rollInitiative(){
        Dice dice = new Dice();
        this.initiative = dice.rollD20() + getDexterity();
        return initiative;
    }

    public int getLevel(){
        return level;
    }
    public void setLevel(int level){
        this.level = level;
    }

    public AbilityModifier getAbilityModifier(){
        return abilityModifier;
    }

    public int getHp(){
        return hp;
    }
    public void setHp(int hp){
        this.hp = hp;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getArmourClass() {
        return armourClass;
    }

    public BackPack getBackpack() {
        return backpack;
    }
    public BackPack getClericalBackpack(){
        return backpack;
    }

    public int gethPOfCreatedPlayer() {
        return hPOfCreatedPlayer;
    }

    public void sethPOfCreatedPlayer(int hPOfCreatedPlayer) {
        this.hPOfCreatedPlayer = hPOfCreatedPlayer;
    }

    @Override
    public String toString() {
        return "--------------------\n" +
                name + "\n" +
                "--------------------\n" +
                "Race\t" + race.getName() + "\n" +
                "Class\t" + gameClass.getNameClass() + "\n" +
                "--------------------\n" +
                "HP\t\t\t" + hp + "\n" +
                "Level\t\t" + level + "\n" +
                "XP\t\t\t" + exp + "\n" +
                "Proficiency\t" + proficiency + "\n" +
                "ArmourClass\t" + armourClass + "\n" +
                "--------------------\n" +
                "Abilities\n" +
                "\n" +
                "Charisma\t\t" + charisma + "\n" +
                "Constitution\t" + constitution + "\n" +
                "Dexterity\t\t" + dexterity + "\n" +
                "Intelligence\t" + intelligence + "\n" +
                "Strength\t\t " + strength + "\n" +
                "Wisdom\t\t\t" + wisdom + "\n" +
                "------------------------------------\n";
    }
}
