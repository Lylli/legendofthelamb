package be.dastudios.legendofthelamb.player.backpack.items.armor;

import be.dastudios.legendofthelamb.player.backpack.items.miscItems.Item;

public class LeatherArmour extends Item {
    private String name = "Leather Armour";
    private String description = "Wanna know why assasins wear leather? Because it's made of hide";
    private int armourBaseLeather = 8;

    public int getArmourBaseLeather() {
        return armourBaseLeather;
    }


    @Override
    public String toString() {
        return " " + name + " - description: " + description;
    }


}
