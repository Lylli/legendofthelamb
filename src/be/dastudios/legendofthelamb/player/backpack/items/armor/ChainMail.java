package be.dastudios.legendofthelamb.player.backpack.items.armor;

import be.dastudios.legendofthelamb.player.backpack.items.miscItems.Item;

public class ChainMail extends Item {
    private String name = "chainmail";
    private String description = "Many small rings make one big strongzo jacket.";
    private int armourBaseChainMail = 10;


    public int getArmourBaseChainMail() {
        return armourBaseChainMail;
    }

    @Override
    public String toString() {
        return " " + name + " - description: " + description;
    }

}
