package be.dastudios.legendofthelamb.itemEncounter;

import be.dastudios.legendofthelamb.Game;
import be.dastudios.legendofthelamb.player.Player;
import be.dastudios.legendofthelamb.player.backpack.items.miscItems.GoldPiece;
import be.dastudios.legendofthelamb.player.backpack.items.miscItems.Item;
import be.dastudios.legendofthelamb.player.backpack.items.miscItems.Spellbook;
import be.dastudios.legendofthelamb.player.gameclass.Healer;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.AskInput;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.Keyboard;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.LayOutText;

import java.io.Serializable;
import java.util.Random;

public class FindItem implements Serializable {
    static private Keyboard keyboard = new Keyboard();
    static private AskInput input = new AskInput();
    public static void seeItem(Player player){
        System.out.println("You see something laying in front of you...");
        if(input.askItemPickUp() == 1){
            generateItem(player);
        }

    }
    public static boolean checkIfHealer(Player player) {
        if (player.getGameClass().getNameClass().equals("Healer")) {
            return true;
        } else {
            return false;
        }
    }

    public static void generateItem(Player player){
        Random random = new Random();
        int chance = random.nextInt(5);
        Item item = new Item();
        switch (chance){
            case 0:
            case 1:
            case 2:
                GoldPiece gp = new GoldPiece();
                System.out.println("You found a gold piece!");
                if (input.askItemKeep() == 1){
                    player.getBackpack().addGpInBackPack(gp.getValueGP());
                    player.getBackpack().printWallet();
                    LayOutText.prettyLines();
                } else {
                    System.out.println("You leave it behind.");
                }
                break;
            case 3:
            case 4:
                Spellbook sb = new Spellbook();
                System.out.println("You found a spellbook!");
                if (input.askItemKeep() == 1){
                    if (checkIfHealer(player)){
                        player.getBackpack().addSpellbookInPackOfKnowledge(sb);
                        player.getBackpack().seePackOfKnowledge();
                    } else {
                        System.out.println("You don't have a pack of knowledge. You are a stupid " + player.getGameClass().getNameClass() + ".");
                    }
                }
                break;
        }
    }
}
