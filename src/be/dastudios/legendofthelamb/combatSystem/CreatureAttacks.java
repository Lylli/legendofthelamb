package be.dastudios.legendofthelamb.combatSystem;

import be.dastudios.legendofthelamb.Game;
import be.dastudios.legendofthelamb.monsters.Monster;
import be.dastudios.legendofthelamb.player.Player;
import be.dastudios.legendofthelamb.player.gameclass.GameClass;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.AskInput;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.Keyboard;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.LayOutText;

import java.io.Serializable;

public class CreatureAttacks implements Serializable {
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RESET = "\u001B[0m";
    private Keyboard keyboard = new Keyboard();
    private AskInput input = new AskInput();
    private EndOfCombat eoc = new EndOfCombat();

    public void monsterAttacksFirst(Monster monster, Player player) {
        System.out.println("The " + ANSI_GREEN + monster.getName() + ANSI_RESET + " is faster than you! The " + ANSI_GREEN + monster.getName() + ANSI_RESET + " attacks first.");
        while (monster.getHP() > 0 && player.getHp() > 0) {
            if (monsterAttackAndDeathCheck(monster, player)) break;
            if (playerMakesChoiceDuringCombat(monster, player)) break;
        }
        if (monster.getHP() <= 0 || player.getHp() <= 0) {
            eoc.deathCreature(monster, player);
        }
    }

    public void playerAttacksFirst(Monster monster, Player player) {
        System.out.println("You are faster than the " + ANSI_GREEN + monster.getName() + ANSI_RESET + "! You attack first.");
        while (monster.getHP() > 0 && player.getHp() > 0) {
            if (playerMakesChoiceDuringCombat(monster, player)) break;
            if (monsterAttackAndDeathCheck(monster, player)) break;
        }
        if (monster.getHP() <= 0 || player.getHp() <= 0) {
            eoc.deathCreature(monster, player);
        }
    }

    public boolean checkIfHealer(Player player) {
        if (player.getGameClass().getNameClass().equals("Healer")) {
            return true;
        } else {
            return false;
        }
    }

    private void playerHeals(Player player) {
        int heal = player.getGameClass().heal();
        int maxhPOfThePlayer = player.gethPOfCreatedPlayer();
        player.setHp((player.getHp() + heal));
        if (player.getHp() > maxhPOfThePlayer){
            player.setHp(maxhPOfThePlayer);
        }
        System.out.println("You healed yourself! Your HP is now " + player.getHp() + ".");

}



    private boolean playerMakesChoiceDuringCombat(Monster monster, Player player) {
        if (checkIfHealer(player)) {
            int choice = input.askHealOrAttack();
            if (choice == 1) {
                if (playerAttackAndDeathCheck(monster, player)) return true;
            } else if (choice == 2) {
                playerHeals(player);
            } else {
                LayOutText.notAnOption();
            }

        return false;
        }
        else {
            int choice = input.askDuringCombatChoice();
            if (choice == 1) {
                if (playerAttackAndDeathCheck(monster, player)) return true;
            }

            else {
                LayOutText.notAnOption();
            }
        }
            return false;
    }


    private boolean playerAttackAndDeathCheck(Monster monster, Player player) {
        attackOfPlayer(player, monster);
        if (isDead(player.getHp()) || isDead(monster.getHP())) {
            return true;
        }
        LayOutText.prettyLines();
        return false;
    }


    private boolean monsterAttackAndDeathCheck(Monster monster, Player player) {
        attackOfMonster(monster, player);
        if (isDead(player.getHp()) || isDead(monster.getHP())) {
            return true;
        }
        LayOutText.prettyLines();
        return false;
    }


    public static void attackOfPlayer(Player player, Monster monster) {
        System.out.println("You try to attack the " + ANSI_GREEN + monster.getName() + ANSI_RESET +"!");
        if (player.getGameClass().attack() > monster.getArmourClass()) {
            int damage = player.getGameClass().damage(monster);
            monster.setHp(monster.getHP() - damage);
            if (monster.getHP() > 0) {
                System.out.println("" + ANSI_GREEN + monster.getName() + ANSI_RESET + " has " + monster.getHP() + "HP left!");
            }
        }
        else {
            System.out.println("You missed!");
        }
    }

    public static void attackOfMonster(Monster monster, Player player){
        if (monster.attack() > player.getArmourClass()){
            int damageMonster = monster.damage();
            player.setHp(player.getHp() - damageMonster);
            if (player.getHp() > 0){
                System.out.println("You have " + player.getHp() + "HP left!");
            }
        }
        else {
            System.out.println("You evaded the attack!");
        }
    }

    public void encounterOfTheLamb(Monster monster, Player player){
        monster.attack();
        int maxhPOfThePlayer = player.gethPOfCreatedPlayer();
        int damageMonster = monster.damage();
        player.setHp(player.getHp() - damageMonster);
        if (player.getHp() > maxhPOfThePlayer){
            player.setHp(maxhPOfThePlayer);
        }
        if (player.getHp() > 0){
            System.out.println("You have " + player.getHp() + "HP left!");
        }
    }

    private boolean isDead(int hp) {
        boolean deadCreature = false;
        if (hp <= 0) {
            deadCreature = true;
        }
        return deadCreature;
    }



}

