package be.dastudios.legendofthelamb.player.race;

public class Human extends Race {
    private final String name = "Human";
    private final int baseSpeed = 6;
    private final int strength = 1;
    private final int constitution = 1;
    private final int wisdom = 1;
    private final int charisma = 1;
    private final int dexterity = 1;
    private final int intelligence = 1;

    private String description = "The human race can be found everywhere in the world of Letla. They have multiple cultures and are aligned to\n" +
            "the entire spectrum of good and evil. There are many noble Paladin warriors, Holy Clerical men and ruthless\n" +
            "fighters to choose from. If you choose to be a Human, you get the following description and benefits.";

    public Human(){
    }

    public String getName(){
        return name;
    }

    public int getBaseSpeed() {
        return baseSpeed;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public int getConstitution() {
        return constitution;
    }

    @Override
    public int getWisdom() {
        return wisdom;
    }

    @Override
    public int getCharisma() {
        return charisma;
    }

    @Override
    public int getDexterity() {
        return dexterity;
    }

    @Override
    public int getIntelligence() {
        return intelligence;
    }


}
