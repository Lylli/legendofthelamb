package be.dastudios.legendofthelamb.monsters;

import be.dastudios.legendofthelamb.player.utility.abilities.Dice;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.LayOutText;

public class Lamb extends Monster {
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_RESET = "\u001B[0m";
    int initiative = 100000;

    public Lamb() {
        setName("Lamb");
        setArmourClass(17);
        setHp(15);

    }


    public int attack() {
        int totalAttack = 1000000;
        try {
            Thread.sleep(2000);
            System.out.println("The" + ANSI_CYAN + " lamb " + ANSI_RESET + " walks up to you.\n");
            Thread.sleep(2000);
            System.out.println("Flashes of light flicker.\n");
            Thread.sleep(2000);
            LayOutText.prettyLines();
        } catch (InterruptedException ie) {
        }
        return totalAttack;
    }

    public int damage() {
        int healPlayer = -10000;
        try {
            Thread.sleep(2000);
            System.out.println("You feel...\n");
            Thread.sleep(2000);
            System.out.println("AMAZING!\n");
            Thread.sleep(2000);
            System.out.println("The" + ANSI_CYAN + " lamb " + ANSI_RESET + " has fully healed you, what an amazing lamb.\n");
            Thread.sleep(2000);
            System.out.println("Some could even say....\n");
            Thread.sleep(5000);
            System.out.println("LEGENDARY!");
            LayOutText.prettyLines();
            Thread.sleep(2000);
        } catch (InterruptedException ie) {
        }
        return healPlayer;
    }

    @Override
    public String toString() {
        return "You see a golden shine at the horizon...." + "\n" +
                "and something fuzzy?" + "\n" +
                "------------------------------------" + "\n" +
                "Is that a lamb?";
    }
}
