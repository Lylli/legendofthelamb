package be.dastudios.legendofthelamb.map;


import be.dastudios.legendofthelamb.Game;
import be.dastudios.legendofthelamb.gameLayOut.Map;
import be.dastudios.legendofthelamb.gameLayOut.Menu;
import be.dastudios.legendofthelamb.player.Player;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.AskInput;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.LayOutText;

import java.io.Serializable;
import java.util.Arrays;

public class ForestOfStreams implements Serializable {
    public Tile[][] mapForest;
    public Tile[][] walkedMap;
    Tile tile;
    Tile endTile;
    private int choice = 1;

    public ForestOfStreams() {
        mapForest = new Tile[40][40];
        for (int i = 0; i < mapForest.length; i++) {
            for (int j = 0; j < mapForest[i].length; j++) {
//TODO: tile (30,1)= room met dorp NPC met quest en accepteren ja/nee? als ja => bugbear gaan doden, questitem/als nee: geen q item an bugbear
//TODO: monsters op tiles zetten
                if ((i == 39 && j == 6) ||
                        (i < 39 && i > 36 && j == 6) ||
                        (i == 37 && j > 6 && j < 9) ||
                        (i < 37 && i > 33 && j == 8) ||
                        (i == 34 && j > 1 && j < 8) ||
                        (i < 35 && i > 21 && j == 1) ||
                        (i == 22 && j > 2 && j < 12) ||
                        (i == 24 && j > 11 && j < 16) ||
                        (i < 31 && i > 24 && j == 11) ||
                        (i > 21 && i < 24 && j == 11) ||
                        (i == 30 && j < 18 && j > 11) ||
                        (i < 30 && i > 25 && j == 17) ||
                        (i == 26 && j <= 21 && j >= 18) ||
                        (i < 36 && i > 25 && j == 22) ||
                        (i == 35 && j < 22 && j > 16) ||
                        (i < 38 && i > 35 && j == 17 ) ||
                        (i == 37 && j > 17 && j < 29) ||
                        (i < 38 && i > 31 && j == 28) ||
                        (i <= 24 && i >= 20 && j == 16) ||
                        (i < 22 && i > 13 && j == 2) ||
                        (i < 13 && i > 2 && j == 2) ||
                        (i == 3 && j > 2 && j < 18) ||
                        (i == 3 && j > 18 && j < 35) ||
                        (i > 3 && i < 32 && j == 34) ||
                        (i == 31 && j == 33) ||
                        (i == 31 && j < 32 && j > 27) ||
                        (i == 32 && j == 32) ||
                        (i == 33 && j < 39 && j > 32) ||
                        (i < 39 && i > 32 && j == 38) ||
                        (i == 38 && j > 29 && j < 39) ||
                        (i < 38 && i > 35 && j == 30) ||
                        (i == 36 && j > 29 && j < 33) ||
                        (i < 37 && i > 33 && j == 32) ||
                        (i < 13 && i > 3 && j == 18) ||
                        (i == 13 && j < 18 && j > 2) ||
                        (i == 14 && j > 17 && j < 27) ||
                        (i == 20 && j > 15 && j < 29)
                ) {
                    mapForest[i][j] = new Tile(i, j, TileType.Path);
                    System.out.print("P ");
                } else if
                ((i == 31 && (j == 32)) ||
                                ((i == 3 || i == 13) && j == 18) ||
                                ((i == 13 || i == 22) && j == 2) ||
                                ((i == 24) && j == 11) ||
                                ((i == 33) && j == 32)) {
                    mapForest[i][j] = new Tile(i, j, TileType.Intersection);
                    System.out.print("I ");
                }


//                de rest zijn trees
                else {
                    mapForest[i][j] = new Tile(i, j, TileType.Trees);
                    System.out.print("- ");
                }
            }
            System.out.println();
        }
    }

    public Tile startTile() {
        System.out.println("You've entered the Forest of Streams.  Before you lies a path, its sides covered by trees.");
        this.tile = mapForest[39][6];
        return tile;
    }

    public Tile getTile() {
        return tile;
    }


    public void setTile(Tile tile) {
        this.tile = tile;
    }

//ik heb de endtile de D(doel) gemaakt op de map in de opdrachtbundel, dit kan nog aangepast worden

    public Tile endTile(){
        endTile = mapForest[26][14];
        return endTile;
    }

//TODO meer monstertiles maken voor deze map
    public boolean isGoblinMinionTile(){
        getTile();
        if ((tile.getX()==37 && tile.getY()==6) || (tile.getX()==35 && tile.getY()==8) || (tile.getX()==34 && tile.getY()==4)){
            return true;
        }
        return false;
    }

    public boolean isGoblinFighterTile(){
        getTile();
        if ((tile.getX() == 32 || tile.getX()==28 || tile.getX()==24) && tile.getY()==1){
            return true;
        }
        return false;
    }

//    Hier tiles maken waar je items kan vinden
    public boolean isItemTile(){
        getTile();
        if (tile.getX() == 38 && tile.getY() == 6){
            return true;
        }
        return false;
    }

    public void printMap(){
//        walkedMap = mapForest
        System.out.println(getTile() + "X ");


    }



    public int getWalkChoice() {
        return choice;
    }

    public void walk() {
        AskInput input = new AskInput();
        this.choice = input.askWhereToWalk();
        while (choice < 0 || choice > 5) {
            System.out.println("Please enter a correct number.");
            this.choice = input.askWhereToWalk();
        }
        int i = tile.getX();
        int j = tile.getY();
        tile= getTile();


            switch (choice) {
                case 1: tile= mapForest[i-1][j];
                        if (tile.type != TileType.Trees) {
                        System.out.println("You go north");
                        tile.setX(i-1);
                        tile.setY(j);
                        this.tile = mapForest[i-1][j];
                        }
                    else {
                        System.out.println("You can't go through trees!");
                        this.tile = mapForest[i][j];
                        }
                    System.out.println(getTile());
                    break;
                case 2:
                    tile = mapForest[i][j + 1];
                    if (tile.type != TileType.Trees) {
                        System.out.println("You go east");
                        tile.setX(i);
                        tile.setY(j+1);
                        this.tile = mapForest[i][j+1];
                    }
                    else {
                        System.out.println("You can't go through trees");
                        tile = mapForest[i][j];
                    }
                    System.out.println(getTile());
                    break;
                case 3:
                    tile = mapForest[i +1][j];
                    if (tile.type != TileType.Trees) {
                        System.out.println("You go south.");
                        tile.setX(i+1);
                        tile.setY(j);
                        this.tile = mapForest[i+1][j];
                    }
                    else {
                    System.out.println("You can't go through trees");
                    tile = mapForest[i][j];
                    }
                    System.out.println(getTile());
                    break;
                case 4:
                    tile = mapForest[i][j - 1];
                    if (tile.type != TileType.Trees) {
                        System.out.println("You go west.");
                        tile.setX(i);
                        tile.setY(j-1);
                        this.tile = mapForest[i][j-1];
                    }
                    else {
                        System.out.println("You can't go through trees");
                        tile = mapForest[i][j];
                    }
                    System.out.println(getTile());
                    break;
                case 5:
                    System.out.println("not available yet");
//                    printMap();
                    break;
                case 0:
//                    setGame(Menu.getGame());
                    Menu.getGame().saveGame();
                    System.out.println("Thank you for visiting Letla, please come again!");
                    LayOutText.prettyLines();
                    break;
            }


    }
}
