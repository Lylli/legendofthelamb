package be.dastudios.legendofthelamb.player.backpack;

import be.dastudios.legendofthelamb.Game;
import be.dastudios.legendofthelamb.player.backpack.items.miscItems.Item;
import be.dastudios.legendofthelamb.player.backpack.items.miscItems.Spellbook;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * This is an abstract class for the different kind of backpacks in the game:
 * at the moment there are 2 different kinds of backpacks,
 * but there is the option to add more in a later stadium of the game development by using this abstract class.
 */
public abstract class BackPack implements Serializable {
    private String name;
    private String description;
    List<Item> backpack = new ArrayList<>();

    public abstract List<Item> seeInventory();

    public abstract void printBackPack();

    public abstract void addGpInBackPack(int gp);

    public abstract void printWallet();

    public abstract void addSpellbookInPackOfKnowledge(Spellbook sb);

    public abstract void seePackOfKnowledge();

}
