package be.dastudios.legendofthelamb.player.gameclass;

import be.dastudios.legendofthelamb.monsters.Monster;
import be.dastudios.legendofthelamb.player.Player;
import be.dastudios.legendofthelamb.player.backpack.BackPack;

/**
 * This is an abstract class for the different kind of classes in the game:
 * at the moment there are 3 different classes: Fighter, Ranger and Healer.
 * There is the option to add more classes in a later stadium of the game development by using this abstract class.
 * Each class has their abilities and an option to attack/deal damage (because this is dependent on which class you choose).
 */
public abstract class GameClass extends Player {

    String nameClass;
    int armourBase;
    int LifeDice;
    String[] items;
    private int strength;
    private int constitution;
    private int wisdom;
    private int charisma;
    private int dexterity;
    private int intelligence;
    private int weaponChoice;

    private BackPack backpack;

    public String getNameClass() {
        return nameClass;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getConstitution() {
        return constitution;
    }

    public void setConstitution(int constitution) {
        this.constitution = constitution;
    }

    public int getWisdom() {
        return wisdom;
    }

    public void setWisdom(int wisdom) {
        this.wisdom = wisdom;
    }

    public int getCharisma() {
        return charisma;
    }

    public void setCharisma(int charisma) {
        this.charisma = charisma;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getArmourBase() {
        return armourBase;
    }

    public void setArmourBase(int armourBase) {
        this.armourBase = armourBase;
    }

    public abstract int attack();

    public abstract int damage(Monster monster);

    public abstract int getWeaponChoice();

    public abstract void setWeaponChoice();

    public abstract int heal();

}
