package be.dastudios.legendofthelamb.player.backpack.items.weapons;

import be.dastudios.legendofthelamb.player.backpack.items.miscItems.Item;

public class Bow extends Item {
    private String name = "Bow";
    private String description = "A piece of wood, stringed with flaxen string. Nothing special, but lethal from a distance";

    public Bow (){
        this.name = "Bow";
    }

    @Override
    public String toString() {
        return " " + name + " - description: " + description;
    }

}
