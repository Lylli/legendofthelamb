package be.dastudios.legendofthelamb.combatSystem;

import be.dastudios.legendofthelamb.Game;
import be.dastudios.legendofthelamb.player.Player;
import be.dastudios.legendofthelamb.monsters.*;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.AskInput;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.Keyboard;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.LayOutText;

import java.io.Serializable;
import java.util.Random;

/**
 * This class lets the player enter combat. It will generate monsters, let monsters attack the player, let the player attack monsters
 * and checks for HP.  If the player dies, it ends the game.
 */
public class Combat implements Serializable {
    private Monster monster;
    private Keyboard keyboard = new Keyboard();
    private AskInput input = new AskInput();
    private CreatureAttacks ca = new CreatureAttacks();

    public void enterCombat(Player player) {
//        generateMonster();
        System.out.println(getMonster());
        LayOutText.prettyLines();
        int ini1 = 0;
        int ini2 = 0;
        if (!(getMonster().getName().equals("Lamb"))) {
            ini1 = getMonster().rollInitiative();
            ini2 = player.rollInitiative();
            if (ini1 >= ini2) {
                ca.monsterAttacksFirst(monster, player);
            } else {
                ca.playerAttacksFirst(monster, player);
            }
        } else {
            ca.encounterOfTheLamb(monster, player);

        }

    }

    public Monster generateGoblinMinion(){
        GoblinMinion gm = new GoblinMinion();
        setMonster(gm);
        return getMonster();
    }
    public Monster generateGoblinFighter(){
        GoblinFighter gm = new GoblinFighter();
        setMonster(gm);
        return getMonster();
    }


    public Monster generateMonster(){
        Random random = new Random();
        int randomMonster = random.nextInt(27);
        switch (randomMonster){
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                GoblinMinion gm = new GoblinMinion();
                setMonster(gm);
                break;
            case 8:
            case 9:
            case 10:
                GoblinRanger gr =  new GoblinRanger();
                setMonster(gr);
                break;
            case 11:
            case 12:
            case 13:
                GoblinFighter gf = new GoblinFighter();
                setMonster(gf);
                break;
            case 14:
            case 15:
                Hobgoblin bigGoblin = new Hobgoblin();
                setMonster(bigGoblin);
                break;
            case 16:
            case 17:
                Bugbear bugbear = new Bugbear();
                setMonster(bugbear);
                break;
            case 18:
                Troll troll = new Troll();
                setMonster(troll);
                break;
            case 19:
                Wolf wolf = new Wolf();
                setMonster(wolf);
                break;
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
                Lamb lamb = new Lamb();
                setMonster(lamb);
                break;


        }
        return getMonster();
    }

    public void setMonster(Monster monster) {
        this.monster = monster;
    }

    public Monster getMonster() {
        return monster;
    }
}
