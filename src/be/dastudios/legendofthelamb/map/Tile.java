package be.dastudios.legendofthelamb.map;

import be.dastudios.legendofthelamb.Game;

import java.io.Serializable;

/**
 * a tile is a square on the drawnout map we got from 'opdrachtenbundel', one tile has its own coordinates and a tiletype
 */
public class Tile implements Serializable {
    private int x,y;
    TileType type;

    public Tile(int x, int y, TileType type){
        this.x=x;
        this.y=y;
        this.type=type;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public TileType getType() {
        return type;
    }

    public void setType(TileType type) {
        this.type = type;
    }


    @Override
    public String toString() {
        return

                "Your coordinates are: "        + x +", "+ y
                ;
    }
}
