package be.dastudios.legendofthelamb;

import be.dastudios.legendofthelamb.gameLayOut.Map;
import be.dastudios.legendofthelamb.gameLayOut.Menu;
import be.dastudios.legendofthelamb.map.ForestOfStreams;
import be.dastudios.legendofthelamb.player.Player;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.AskInput;

import java.io.*;

public class Game implements Serializable {
    private Player player;
    private ForestOfStreams map;
    private AskInput input = new AskInput();
    private static File folder = new File("D:\\Opleiding\\Java\\Project 2\\Project");

    public Game (Player player, ForestOfStreams map){
        this.player = player;
        this.map = map;
    }

    public void saveGame(){
        String name = input.getNameSavegame();
        try {
            FileOutputStream fos = new FileOutputStream(name +".sav");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(this);
            oos.flush();
            oos.close();
            System.out.println("Game saved\n");
        } catch (Exception e){
            e.getMessage();
        }
    }

//    private static void checkForSavegames(){
//        if (printSavegames()){
//            loadGame();
//        }
//    }

    public static Game loadGame() throws Exception{
        AskInput input = new AskInput();
        String name = input.chooseSavegame();
        FileInputStream fis = new FileInputStream(name +".sav");
        ObjectInputStream ois = new ObjectInputStream(fis);
        Game temp = (Game) ois.readObject();
        ois.close();
        System.out.println("Game loaded");
        return temp;
    }

    public static boolean printSavegames() {
        Boolean savedGames = false;
//        File[] listOfFiles = folder.listFiles();
//
//        for (int i = 0; i < listOfFiles.length; i++) {
//            if (listOfFiles[i].isFile()) {
//                System.out.println("File " + listOfFiles[i].getName());
//            } else if (listOfFiles[i].isDirectory()) {
//                System.out.println("Directory " + listOfFiles[i].getName());
//            }
//        }
        FilenameFilter textFilefilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                String lowercaseName = name.toLowerCase();
                return lowercaseName.endsWith(".sav");
            }
        };
        String textFilesList[] = folder.list(textFilefilter);
        if (textFilesList.length > 0) {
            savedGames = true;
            System.out.println("List of the savegames:");
            for (String fileName : textFilesList) {
                String name = fileName.replaceFirst(".sav", "");
                System.out.println(name);
            }
        } else {
            System.out.println("There are no saved games... yet.");
        }
        return savedGames;
    }


    public Player getPlayer() {
        return player;
    }
//
//    public void setPlayer(Player player) {
//        this.player = player;
//    }
//
    public ForestOfStreams getMap() {
        return map;
    }
//
//    public void setMap(ForestOfStreams map) {
//        this.map = map;
//    }
}
