package be.dastudios.legendofthelamb.player.gameclass;

import be.dastudios.legendofthelamb.monsters.Monster;
import be.dastudios.legendofthelamb.player.utility.abilities.AbilityCalculator;
import be.dastudios.legendofthelamb.player.utility.abilities.AbilityModifier;
import be.dastudios.legendofthelamb.player.utility.abilities.Dice;
import be.dastudios.legendofthelamb.player.backpack.RegularBackPack;
import be.dastudios.legendofthelamb.player.backpack.items.armor.LeatherArmour;
import be.dastudios.legendofthelamb.player.backpack.items.weapons.Bow;
import be.dastudios.legendofthelamb.player.backpack.items.weapons.Sword;
import be.dastudios.legendofthelamb.player.utility.textAndStuff.Keyboard;

import java.util.ArrayList;
import java.util.Scanner;

public class Ranger extends GameClass {
    private final String nameClass = "Ranger";
    private int lifeDiceOutcome;
    private int armourBase = 6;
    private int weaponChoice;

    private int strength;
    private int constitution;
    private int wisdom;
    private int charisma;
    private int dexterity;
    private int intelligence;
    private RegularBackPack backpack = new RegularBackPack();
    private int wallet;


    public Ranger (){
        this.lifeDiceOutcome = getHP();
        this.backpack = startersBackpack();
    }
    public RegularBackPack startersBackpack(){
        Sword sword = new Sword();
        Bow bow = new Bow();
        LeatherArmour la = new LeatherArmour();
        this.armourBase += la.getArmourBaseLeather();
        this.backpack.addItemInBackPack(sword);
        this.backpack.addItemInBackPack(bow);
        this.backpack.addItemInBackPack(la);
        this.wallet = 10;
        this.backpack.addGpInBackPack(wallet);
        return backpack;
    }

    @Override
    public String getNameClass() {
        return nameClass;
    }

    public RegularBackPack getBackpack() {
        return backpack;
    }

    public int getHP(){
        int total = 0;
        for (int k = 0; k<3; k++){
            Dice lifeDice = new Dice();
            total += lifeDice.rollD8();
        }
        return total +10;
    }

    public void setAbilityValuesStandard(){
        dexterity = 16 + 2;
        intelligence = 14;
        constitution = 14 + 1;
        wisdom = 13;
        charisma = 11;
        strength = 8;
    }

    public void setAbilityValuesVariable() {
        AbilityCalculator ac = new AbilityCalculator();
        ArrayList<Integer> abilityValues = ac.finalVariableValues();
        dexterity = abilityValues.get(0) + 2;
        intelligence = abilityValues.get(1) + 1;
        constitution = abilityValues.get(2);
        wisdom = abilityValues.get(3);
        charisma = abilityValues.get(4);
        strength = abilityValues.get(5);
    }

    public int getLifeDiceOutcome() {
        return lifeDiceOutcome;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getConstitution() {
        return constitution;
    }

    public void setConstitution(int constitution) {
        this.constitution = constitution;
    }

    public int getWisdom() {
        return wisdom;
    }

    public void setWisdom(int wisdom) {
        this.wisdom = wisdom;
    }

    public int getCharisma() {
        return charisma;
    }

    public void setCharisma(int charisma) {
        this.charisma = charisma;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getArmourBase(){
        return armourBase;
    }

    @Override
    public void setWeaponChoice(){
        Keyboard keyboard = new Keyboard();
        int weaponChoice = keyboard.readInt("Would you like to attack with your sword(1) or your bow and arrow(2)?");
        while (weaponChoice < 1 || weaponChoice >2){
            System.out.println("Please enter 1 or 2.");
            weaponChoice = keyboard.readInt("Would you like to attack with your sword(1) or your bow and arrow(2)?");
        }
        this.weaponChoice = weaponChoice;
    }

    @Override
    public int heal() {
        return 0;
    }

    @Override
    public int getWeaponChoice() {
        return weaponChoice;
    }

    public int attack(){
        setWeaponChoice();
        int choice = getWeaponChoice();
        int totalAttack = 0;
        if (choice == 1) {
            switch (getLevel()) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    totalAttack = swordAttack();
                    break;
            }
        } else {
            switch (getLevel()) {
                case 1:
                case 2:
                case 3:
                case 5:
                    totalAttack = bowAttack();
                    break;
                case 4:
                    totalAttack = 1000000000;
                    break;
            }
        }
        return totalAttack;
    }

    private int swordAttack() {
        int totalAttack;
        Dice dice = new Dice();
        AbilityModifier abilityModifier = new AbilityModifier(getStrength());
        int levelAttack = dice.rollD20() + getProficiency() + abilityModifier.useModifier();
        int sword = dice.rollD20() + getProficiency() + getStrength();
        totalAttack = (sword + levelAttack) / 2;
        return totalAttack;
    }

    private int swordDamage(){
        int totalDamage;
        Dice dice = new Dice();
        AbilityModifier abilityModifier = new AbilityModifier(getStrength());
        int levelDamage = 0;
        do {
            levelDamage = dice.rollD10() + abilityModifier.useModifier();
        } while (levelDamage == 0);
        int sword = dice.rollD10() + getStrength();
        totalDamage = sword / levelDamage;
        System.out.println("You hit with the sword! Tjakaa!!");
        return totalDamage;
    }

    private int bowAttack(){
        int totalAttack;
        Dice dice = new Dice();
        AbilityModifier abilityModifier = new AbilityModifier(getDexterity());
        int levelAttack = dice.rollD20() + getProficiency() + abilityModifier.useModifier();
        int bow = dice.rollD20() + getProficiency() + getDexterity();
        totalAttack = (bow + levelAttack)/2;
        return totalAttack;
    }

    private int bowDamage(){
        int totalDamage;
        Dice dice = new Dice();
        AbilityModifier abilityModifier = new AbilityModifier(getDexterity());
        int levelDamage = dice.rollD12() + abilityModifier.useModifier();
        int bow = dice.rollD12() + getDexterity();
        totalDamage = bow / levelDamage;
        System.out.println("You hit with an arrow! Zooof!");
        return totalDamage;
    }

    private int piercingShot(){
        int totalDamage;
        Dice dice = new Dice();
        AbilityModifier abilityModifier = new AbilityModifier(getDexterity());
        int levelDamage = dice.rollD12() + (2 * abilityModifier.useModifier());
        int bow = dice.rollD12() + getDexterity();
        totalDamage = bow / levelDamage;
        System.out.println("You hit with an arrow! Zooof!");
        return totalDamage;
    }

    private int seekerShot(){
        int totalDamage;
        Dice dice = new Dice();
        AbilityModifier abilityModifier = new AbilityModifier(getDexterity());
        int levelDamage = (dice.rollD12() + abilityModifier.useModifier())/2;
        int bow = dice.rollD12() + getDexterity();
        totalDamage = bow / levelDamage;
        System.out.println("You hit with an arrow! Zooof!");
        return totalDamage;
    }

    private int arrowRain(){
        int totalDamage;
        Dice dice = new Dice();
        AbilityModifier abilityModifier = new AbilityModifier(getDexterity());
//        TODO multiple targets
        int levelDamage = dice.rollD10() + abilityModifier.useModifier();
        int bow = dice.rollD12() + getDexterity();
        totalDamage = bow / levelDamage;
        System.out.println("You hit with an arrow! Zooof!");
        return totalDamage;
    }

    public int damage(Monster monster){
        Keyboard keyboard = new Keyboard();
        int totalDamage = 0;
        int choice = getWeaponChoice();
        if (choice == 1){
        switch (getLevel()) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                totalDamage = swordDamage();
                break;
        }

        } else {
            switch (getLevel()) {
                case 1:
                case 2:
                    totalDamage = bowDamage();
                    break;
                case 3:
                    int attackChoice = keyboard.readInt("Please choose your attack: \n" +
                            "1. Bow attack\n" +
                            "2. Piercing shot\n");
                    while (attackChoice <1 || attackChoice > 2){
                        System.out.println("Please choose a valid number.\n");
                        attackChoice = keyboard.readInt("Please choose your attack: \n" +
                                "1. Bow attack\n" +
                                "2. Piercing shot\n");
                    }
                    if (attackChoice == 1){
                        totalDamage = bowDamage();
                    } else {
                        totalDamage = piercingShot();
                    }
                    break;
                case 4:
                    attackChoice = keyboard.readInt("Please choose your attack: \n" +
                            "1. Bow attack\n" +
                            "2. Piercing shot\n" +
                            "3. Seeker shot\n");
                    while (attackChoice <1 || attackChoice > 3){
                        System.out.println("Please choose a valid number.\n");
                        attackChoice = keyboard.readInt("Please choose your attack: \n" +
                                "1. Bow attack\n" +
                                "2. Piercing shot\n" +
                                "3. Seeker shot\n");
                    }
                    if (attackChoice == 1){
                        totalDamage = bowDamage();
                    } else if (attackChoice == 2) {
                        totalDamage = piercingShot();
                    } else {
                        totalDamage = seekerShot();
                    }
                    break;
                case 5:
                    attackChoice = keyboard.readInt("Please choose your attack: \n" +
                            "1. Bow attack\n" +
                            "2. Piercing shot\n" +
                            "3. Seeker shot\n" +
                            "4. Arrow rain\n");
                    while (attackChoice <1 || attackChoice > 4){
                        System.out.println("Please choose a valid number.\n");
                        attackChoice = keyboard.readInt("Please choose your attack: \n" +
                                "1. Bow attack\n" +
                                "2. Piercing shot\n" +
                                "3. Seeker shot\n" +
                                "4. Arrow rain");
                    }
                    if (attackChoice == 1){
                        totalDamage = bowDamage();
                    } else if (attackChoice == 2) {
                        totalDamage = piercingShot();
                    } else if(attackChoice == 3) {
                        totalDamage = seekerShot();
                    } else {
                        totalDamage = arrowRain();
                    }
                    break;
            }
        }
        return totalDamage;
    }


    @Override
    public String toString() {
        return "Ranger{" +
                "nameClass='" + nameClass + '\'' +
                ", lifeDiceOutcome=" + lifeDiceOutcome +
                ", dexterity=" + dexterity +
                ", intelligence=" + intelligence +
                ", constitution=" + constitution +
                ", wisdom=" + wisdom +
                ", charisma=" + charisma +
                ", strength=" + strength +
                ", armourbase= " + armourBase +
                '}';
    }
}
