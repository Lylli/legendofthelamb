package be.dastudios.legendofthelamb.player.backpack.items.miscItems;
/**
 * This is the currency of the game.
 * You can get goldpieces by :
 * - creating a character (in the backpack)
 * - loot from monsters
 * - rewards from quests (not yet available)
 * @see be.dastudios.legendofthelamb.player.backpack.BackPack
 */
public class GoldPiece extends Item{
    private String name = "GP";
    private int valueGP = 1;
    private int sumGP;


    public String getName(){
        return name;
    }

    public void setValueGP(int valueGP) {
        this.valueGP = valueGP;
    }

    public int getValueGP(){
        return valueGP;
    }

    public int getSumGP(){
        return sumGP += valueGP;
    }

    @Override
    public String toString() {
        return "GoldPiece{" +
                "TotalGP=" + sumGP +
                '}';
    }
}
